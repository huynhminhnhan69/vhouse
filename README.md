Start Project
* FE : npm run dev
* BE : 
    - Run composer i
    - Run php artisan migrate
    - Run php artisan migrate // to create database structure.
    - Run php artisan cms:user: // create to create admin user.
    - Run php artisan cms:theme:activate ripple // active default theme
    - Run php artisan vendor:publish --tag=cms-public --force
    - Run php artisan cms:theme:assets:publish
    - Run php artisan serve
       
* Run web locally:

- Change APP_URL in .env to APP_URL=http://localhost:8000

- Run php artisan serve. 

- Open http://localhost:8000, you should see the homepage.

- Go to /admin to access to admin panel.



