import { currentUser } from "../../helpers/auth";
const user = currentUser();

export default {
  state: {
    currentUser: user,
    isLoggedIn: !!user,
    loading: false,
    authError: null,
    token_expired : false
  },
  getters: {
    IS_LOADING: state => {
      return state.loading;
    },
    IS_LOGGEND_IN: state => {
      return state.isLoggedIn;
    },
    CURRENT_USER: state => {
      return state.currentUser;
    },
    token_expired: state => {
      return state.token_expired;
    },
    AUTH_ERROR: state => {
      return state.authError;
    }
  },
  mutations: {
    LOGIN: state => {
      state.loading = true;
      state.authError = null;
    },
    LOGIN_SUCCESS: (state, payload) => {
      state.authError = null;
      state.isLoggedIn = true;
      state.loading = false;
      state.token_expired = payload.expires_in
      state.currentUser = Object.assign({}, payload.user, {
        token: payload.user
      });
      localStorage.setItem("token", payload.access_token);
    },
    LOGIN_FAILED: (state, payload) => {
      state.authError = payload.err;
      state.loading = false;
    },
    LOGOUT: state => {
      localStorage.removeItem("token");
      state.isLoggedIn = false;
      state.currentUser = null;
    }
  },
  actions: {
    LOGIN: context => {
      context.commit("LOGIN");
    },
    LOGOUT: context => {
      context.commit("LOGOUT");
    }
  }
};
