// import API from "../api/api";
import axios from "axios";

export function login(credential) {
  return new Promise((res, rej) => {
    axios
      .post("http://localhost:8000/api/v1/users/login", credential)
      .then(result => {
        res(result.data);
      })
      .catch(err => {
        rej(err);
      });
  });
}

export function register(credential) {
  return new Promise((res, rej) => {
    this.$http
      .post("/api/auth/register", credential)
      .then(result => {
        res(result.data);
      })
      .catch(err => {
        rej(err);
      });
  });
}

export function currentUser() {
  const token = localStorage.getItem("token");

  if (!token) {
    return null;
  }

  return token;
}
