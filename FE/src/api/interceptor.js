import store from '../store'

const checkToken =  (instance) => {
    instance.interceptors.response.use(
        (response) => {
            if (response.headers['content-type'].startsWith('application/json')) {
                return response
              }
        },
        error => {
            if(error.response.status == 401 ) {
                store.dispatch("LOGOUT");
                window.location.href = window.location.origin + '/login'
            }
            if(error.response.status == 400 ) {
                return Promise.reject(error.response.statusText)
            }
            return Promise.reject(error)
                
                // this.$router.push({ path: "/login" });
            
        }
    )
}
export default {
    checkToken
  }