import axios from "axios";
import interceptor from "./interceptor";

const API =  axios.create({
  baseURL: `http://127.0.0.1:8000`,
  headers: {
    "Content-Type": "application/json",
    Authorization: "Bearer" +" " + localStorage.getItem("token"),
    Accept: "*/*"
  },
  timeout: 5000 // request timeout

},
);
// API.headers.Authorization = "Bearer" +" " + localStorage.getItem("token");
// debugger;
interceptor.checkToken(API);
export default API

