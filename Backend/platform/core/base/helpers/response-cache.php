<?php
use Spatie\ResponseCache\Facades\ResponseCache;

if (!function_exists('clean_response_cache')) {
    /**
     * [clean_response_cache description]
     * 
     * @author TrinhLe
     */
    function clean_response_cache(){
        ResponseCache::clear();
    }
}