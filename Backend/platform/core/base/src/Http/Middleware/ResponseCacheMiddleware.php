<?php

namespace Botble\Base\Http\Middleware;

use Spatie\ResponseCache\Middlewares\CacheResponse;
use Closure;
use Illuminate\Http\Request;
use Spatie\ResponseCache\ResponseCache;
use Spatie\ResponseCache\Events\CacheMissed;
use Symfony\Component\HttpFoundation\Response;
use Spatie\ResponseCache\Events\ResponseCacheHit;

class ResponseCacheMiddleware extends CacheResponse
{
	
}