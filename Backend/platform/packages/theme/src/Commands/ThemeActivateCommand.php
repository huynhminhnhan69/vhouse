<?php

namespace Botble\Theme\Commands;

use Botble\Setting\Supports\SettingStore;
use Botble\Theme\Commands\Traits\ThemeTrait;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem as File;
use Illuminate\Support\Arr;
use Botble\Base\Commands\PluginActivateCommand;

class ThemeActivateCommand extends Command
{

    use ThemeTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cms:theme:activate 
        {name : The theme that you want to activate} 
        {--path= : Path to theme directory}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate a theme';

    /**
     * @var File
     */
    protected $files;

    /**
     * @var SettingStore
     */
    protected $settingStore;

    /**
     * @var ThemeAssetsPublishCommand
     */
    protected $themeAssetsPublishCommand;

    protected $pluginActivateCommand;

    /**
     * Create a new command instance.
     *
     * @param \Illuminate\Filesystem\Filesystem $files
     * @param SettingStore $settingStore
     * @param ThemeAssetsPublishCommand $themeAssetsPublishCommand
     */
    public function __construct(
        File $files,
        SettingStore $settingStore,
        ThemeAssetsPublishCommand $themeAssetsPublishCommand,
        PluginActivateCommand $pluginActivateCommand
    )
    {
        $this->files = $files;
        $this->settingStore = $settingStore;
        $this->themeAssetsPublishCommand = $themeAssetsPublishCommand;
        $this->pluginActivateCommand = $pluginActivateCommand;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * 
     */
    public function handle()
    {
        if (!preg_match('/^[a-z0-9\-]+$/i', $this->argument('name'))) {
            $this->error('Only alphabetic characters are allowed.');
            return false;
        }

        if (!$this->files->isDirectory($this->getPath(null))) {
            $this->error('Theme "' . $this->getTheme() . '" is not exists.');
            return false;
        }

        $this->settingStore
            ->set('theme', $this->getTheme())
            ->save();

        // Active require plugin for theme
        $theme = $this->settingStore->get('theme');

        $content = get_file_data(theme_path($theme . '/theme.json'));
        
        if (!empty($content)) {
            $required_plugins = Arr::get($content, 'required_plugins', []);
            if (!empty($required_plugins)) {
                $this->info('Activating required plugins ...');
                foreach ($required_plugins as $required_plugin) {
                    $this->info('Activating plugin "' . $required_plugin . '"');
                    $this->call($this->pluginActivateCommand->getName(), ['name' => $required_plugin]);
                }
            }
        }


        $this->call($this->themeAssetsPublishCommand->getName(), ['name' => $this->getTheme()]);
        $this->info('Activate theme ' . $this->argument('name') . ' successfully!');
        $this->call('cache:clear');
        return true;
    }
}
