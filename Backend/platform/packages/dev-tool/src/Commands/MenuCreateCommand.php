<?php

namespace Botble\DevTool\Commands;

use AclManager;
use Botble\Menu\Repositories\Interfaces\MenuInterface;
use Botble\Menu\Repositories\Interfaces\MenuLocationInterface;
use Botble\Menu\Repositories\Interfaces\MenuNodeInterface;
use Botble\Menu\Http\Requests\MenuRequest;
use Botble\Base\Events\CreatedContentEvent;
use Exception;
use Illuminate\Console\Command;
use Validator;

class MenuCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:menu:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Menu';

    /**
     * @var mixed
     */
    protected $menuRepository;

    /**
     * @var MenuNodeInterface
     */
    protected $menuNodeRepository;

    /**
     * @var MenuLocationInterface
     */
    protected $menuLocationRepository;

    /**
     * Install constructor.
     * @param UserInterface $userRepository
     *
     */
    public function __construct( MenuInterface $menu, MenuNodeInterface $menuNodeRepository, MenuLocationInterface $menuLocationRepository)
    {
        parent::__construct();

        $this->menuRepository = $menu;
        $this->menuNodeRepository = $menuNodeRepository;
        $this->menuLocationRepository = $menuLocationRepository;
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {
        $this->createMenu();
    }

    /**
     * Create a superuser.
     *
     * @return bool
     *
     */
    protected function createMenu()
    {
        $this->info('Creating Menu...');

        $menuNodeArray =[
            [
                'name' => 'Câu chuyện về Fomilac',
                'nameEn' => 'About Fomilac',
                'url' => 've-parimilk'
            ],
            [
                'name' => 'Sản phẩm Fomilac',
                'nameEn' => 'Product Fomilac',
                'url' => '#',
                'child' => [
                    [
                        'name' => 'Sản phẩm',
                        'nameEn' => 'Product',
                        'url' => 'san-pham'
                    ],
                    [
                        'name' => 'Thông tin công bố sản phẩm',
                        'nameEn' => 'Product Announcement Information',
                        'url' => 'thong-tin-cong-bo-san-pham'
                    ]
                ]
            ],
            [
                'name' => 'Dinh dưỡng chuẩn Châu Âu',
                'nameEn' => 'EU Nutritional Quality',
                'url' => '#',
                'child' => [
                    [
                        'name' => 'Cho trẻ em',
                        'nameEn' => 'For Baby',
                        'url' => 'dinh-duong-chuan-chau-au?cate=cho-tre-em'
                    ],
                    [
                        'name' => 'Cho mẹ',
                        'nameEn' => 'For Mom',
                        'url' => 'dinh-duong-chuan-chau-au?cate=cho-me'
                    ],
                    [
                        'name' => 'Cho người lớn',
                        'nameEn' => 'For Adult',
                        'url' => 'dinh-duong-chuan-chau-au?cate=cho-nguoi-lon'
                    ],
                    [
                        'name' => 'Cho xương chắc khoẻ',
                        'nameEn' => 'For Bone',
                        'url' => 'dinh-duong-chuan-chau-au?cate=cho-xuong'
                    ]
                ]
            ],
            [
                'name' => 'Góc chuyên gia',
                'nameEn' => 'Expert Corner',
                'url' => 'goc-chuyen-gia'
            ],
            [
                'name' => 'Kiểm tra sức khoẻ',
                'nameEn' => 'Health Check',
                'url' => 'kiem-tra-suc-khoe'
            ],
            [
                'name' => 'Tin tức & Sự kiện',
                'nameEn' => 'News & Events',
                'url' => 'tin-tuc-su-kien'
            ],
            [
                'name' => 'Liên hệ',
                'nameEn' => 'Contact Information',
                'url' => 'lien-he'
            ]
        ];

        try {
            //create menu
            $this->menuRepository->getModel()->truncate();
            $menu = $this->menuRepository->getModel();
            $menu->name = 'Menu';
            $menu->slug = $this->menuRepository->createSlug($menu->name);
            $menu->status = 'published';
            $menuRequestVi = new MenuRequest(['name' => 'Menu','language' => 'vi']);
            $menuRequestEn = new MenuRequest(['name' => 'Menu','language' => 'en_US']);
            $menuVi = $this->menuRepository->createOrUpdate($menu);
            event(new CreatedContentEvent(MENU_MODULE_SCREEN_NAME, $menuRequestVi, $menuVi));

            $menu = $this->menuRepository->getModel();
            $menu->name = 'Menu';
            $menu->slug = $this->menuRepository->createSlug($menu->name);
            $menu->status = 'published';
            $menu->slug = $this->menuRepository->createSlug($menu->name);
            $menuEn = $this->menuRepository->createOrUpdate($menu);
            event(new CreatedContentEvent(MENU_MODULE_SCREEN_NAME, $menuRequestEn, $menuEn));


            $menuLocation = $this->menuLocationRepository->getModel();
            $menuLocation->location = 'main-menu';
            $this->menuLocationRepository->getModel()->truncate();
            $menuLocation->menu_id = $menuVi->id;
            $this->menuLocationRepository->createOrUpdate($menuLocation);
            event(new CreatedContentEvent(MENU_LOCATION_MODULE_SCREEN_NAME, $menuRequestVi, $menuLocation));

            $menuLocation = $this->menuLocationRepository->getModel();
            $menuLocation->location = 'main-menu';
            $menuLocation->menu_id = $menuEn->id;
            $this->menuLocationRepository->createOrUpdate($menuLocation);
            event(new CreatedContentEvent(MENU_LOCATION_MODULE_SCREEN_NAME, $menuRequestEn, $menuLocation));

            $this->menuNodeRepository->getModel()->truncate();
            $this->createNode($menuNodeArray, $menuVi->id, 0);
            $this->createNode($menuNodeArray, $menuEn->id, 0, 'en');


            $this->info('Menu is created.');
        } catch (Exception $exception) {
            $this->error('Menu could not be created.');
            $this->error($exception->getMessage());
        }

        return true;
    }

    function createNode($menuNodeArray, $menu_id, $parent_id, $lang = 'vi'){
        foreach ($menuNodeArray as $key => $parent){
            $node = $this->menuNodeRepository->getModel();
            $node->menu_id = $menu_id;
            $node->parent_id = $parent_id;
            $node->related_id = 0;
            $node->type = 'custom-link';
            $node->position = $key;
            $node->title = $lang == 'vi' ? $parent['name'] : $parent['nameEn'] ;
            $node->target = '_self';
            $node->url = $lang == 'vi' ? $parent['url'] : 'en/' .$parent['url'];
            $node->has_child = isset($parent['child']) ? 1 : 0;
            $node = $this->menuNodeRepository->createOrUpdate($node);
            if ($node->has_child){
                $this->createNode($parent['child'], $menu_id, $node->id, $lang);
            }
        }
    }
}
