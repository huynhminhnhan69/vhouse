<?php

return [
    // List supported modules or plugins
    'supported' => [
        'gallery',
        'page',
        'post',
        'product',
        'payment_transaction',
        'buy_in_credit'
    ],
];