/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./platform/plugins/tenant/resources/assets/js/tenantForm.js":
/*!*******************************************************************!*\
  !*** ./platform/plugins/tenant/resources/assets/js/tenantForm.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return tenantForm; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var tenantForm = /*#__PURE__*/function () {
  function tenantForm() {
    _classCallCheck(this, tenantForm);

    this.timeout = timeout;
  }

  _createClass(tenantForm, [{
    key: "prepareData",
    value: function prepareData() {
      var Model = {
        'id': '',
        'name': '',
        'email': '',
        'phone': '',
        'avatar': '',
        'cmnd_image': '',
        'cmnd_number': '',
        'cmnd_date_create': '',
        'cmnd_place': '',
        'birth_day': '',
        'parent_id': '',
        'room_forrent_id': '',
        'tenant_type': '',
        'province_id': '',
        'district_id': '',
        'ward_id': '',
        'family_record_book': '',
        'job': '',
        'work_place': '',
        'sex': '',
        'parent_name': '',
        'parent_phone': '',
        'note': '',
        'status': ''
      };
    }
  }, {
    key: "toQueryString",
    value: function toQueryString() {
      var serialized = $form.serialize();
      console.log(serialized);
    }
  }, {
    key: "validationForm",
    value: function validationForm(data, configs, validateSubmitInventory) {
      var doValidationInputForm = function doValidationInputForm(data, configs) {
        var errors = [];
        configs.forEach(function (conf, index) {
          var appendValidation = true;

          if (conf.appendRules) {
            appendValidation = conf.appendRules(data);
          }

          var inputName = conf['inputName'];
          var messageKey = conf['messageKey'] || inputName;
          var validation = !data[inputName] && appendValidation;

          if (validation) {
            errors.push(messageKey);
          }
        });
        return errors;
      };

      var getMessage = function getMessage(errors) {
        errors.forEach(function (messageKey) {
          var messages = validateSubmitInventory;
          return toastr.warning("<span class=\"remixicon-error-warning-line\">".concat(messages[messageKey], "</span><span></span>"));
        });
      };

      var errors = doValidationInputForm(data, configs);

      if (errors.length > 0) {
        getMessage(errors);
        throw 'validation request';
      }
    }
  }, {
    key: "CreateOrUpdateTennant",
    value: function CreateOrUpdateTennant(data, type) {
      prepareData(); // validationForm();
    }
  }]);

  return tenantForm;
}();



/***/ }),

/***/ 0:
/*!*************************************************************************!*\
  !*** multi ./platform/plugins/tenant/resources/assets/js/tenantForm.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/huynhminhnhan/Documents/Chouse/vhouse/Backend/platform/plugins/tenant/resources/assets/js/tenantForm.js */"./platform/plugins/tenant/resources/assets/js/tenantForm.js");


/***/ })

/******/ });