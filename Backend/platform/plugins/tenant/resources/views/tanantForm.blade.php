
<div class="modal fade" id="tanantModel" tabindex="-1" role="dialog" aria-labelledby="tanantModel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Thêm khách</h4>
      </div>
      <div class="modal-body">
        
<form class="simple_form form-horizontal" autocomplete="off" novalidate="novalidate" id="FormTenant" data-route="{{ route('tenant.create') }}" enctype="multipart/form-data" accept-charset="UTF-8" data-remote="true" method="post">
  <div class="renter-action-new" style="display: block;">
    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label">Tên khách thuê<span class="required" aria-required="true"> * </span>
      </label>
      <div class="col-md-4">
        <input class="string required bg-maxlength form-control" placeholder="Nhập tên khách thuê" maxlength="255" size="255" type="text" name="name" id="admin_renter_name">
        <div class="form-control-focus"></div>
      </div>
      <label class="col-md-2 control-label">Khách thuê</label>
      <div class="col-md-4">
        <div class="md-radio-inline">
          <div class="md-radio">
            <input type="radio" id="radio_action_type0" name="action_type" checked="" class="md-radiobtn renter-action-type" value="0">
            <label for="radio_action_type0">
              <span class="inc"></span>
              <span class="check"></span>
              <span class="box"></span>Thêm mới</label>
          </div>
          <div class="md-radio">
            <input type="radio" id="radio_action_type1" name="action_type" class="md-radiobtn renter-action-type" value="1">
            <label for="radio_action_type1">
              <span class="inc"></span>
              <span class="check"></span>
              <span class="box"></span>Đã ở</label>
          </div>
        </div>
      </div>
    </div>
    <!-- Sdt, email-->
    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label">SĐT (VN, QT)
        <span class="required" aria-required="true"> </span></label>
      <div class="col-md-4">
        <input class="string tel required bg-maxlength form-control phone-number" placeholder="Nhập SĐT" maxlength="50" type="tel" size="50" name="phone" id="admin_renter_phone">
        <div class="form-control-focus"></div>
      </div>

      <label class="col-md-2 control-label">Email</label>
      <div class="col-md-4">
        <input class="string email optional bg-maxlength form-control" placeholder="Nhập email" maxlength="255" type="email" size="255" name="email" id="admin_renter_email">
        <div class="form-control-focus"></div>
      </div>
    </div>
    <!-- Ngay sinh, gioi tinh -->
    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label">Ngày sinh </label>
      <div class="col-md-4">
        <div class="input-group date datepicker select-date" data-date-format="dd/mm/yyyy">
          <input type="text" name="birth_day" value="" class="form-control" readonly="">
          <span class="input-group-btn">
            <button class="btn default" type="button">
                <i class="fa fa-calendar"></i>
            </button>
          </span>
        </div>
      </div>
      <label class="col-md-2 control-label">Giới tính</label>
      <div class="col-md-4">
        <div class="md-radio-inline">
          <div class="md-radio">
            <input type="radio" id="radio_renter_sex1" name="sex" checked class="md-radiobtn" value="0">
            <label for="radio_renter_sex1">
              <span></span>
              <span class="check"></span>
              <span class="box"></span> Nam </label>
          </div>
          <div class="md-radio">
            <input type="radio" id="radio_renter_sex2" name="sex" class="md-radiobtn" value="1">
            <label for="radio_renter_sex2">
              <span></span>
              <span class="check"></span>
              <span class="box"></span> Nữ </label>
          </div>
        </div>
        <div class="form-control-focus"></div>
      </div>
    </div>
    <hr>
    <!-- so cmnd/cccd, ngay cap -->
    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label">Số CMND/CCCD
        <span class="required" aria-required="true"> </span></label>
      <div class="col-md-4">
        <input class="string required bg-maxlength form-control identity-number" placeholder="Nhập số CMND/CCCD" maxlength="50" size="50" type="text" name="identity_card" id="admin_renter_identity_card">
        <div class="form-control-focus"></div>
      </div>
  
      <label class="col-md-2 control-label required">Ngày cấp</label>
      <div class="col-md-4">
        <div class="input-group date datepicker select-date" data-date-format="dd/mm/yyyy">
          <input type="text" name="date_of_issue" value="" data-date-format="dd/mm/yyyy" class="form-control" readonly="">
          <span class="input-group-btn">
            <button class="btn default" type="button">
                <i class="fa fa-calendar"></i>
            </button>
          </span>
        </div>
      </div>
    </div>
    <!-- noi cap cmnd, Ho khau -->
    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label required">Nơi cấp</label>
      <div class="col-md-4">
        <input class="string optional bg-maxlength form-control" placeholder="Nhập nơi cấp" maxlength="255" size="255" type="text" name="issued_place" id="admin_renter_issued_card">
        <div class="form-control-focus"></div>
      </div>

      <label class="col-md-2 control-label">Hộ khẩu</label>
      <div class="col-md-4">
        <textarea class="text optional bg-maxlength form-control autosizeme" placeholder="Nhập hộ khẩu" maxlength="500" name="work_place" id="work_place"></textarea>
        <div class="form-control-focus"></div>
      </div>
    </div>
    <hr>
    <!-- nghe nghiep, truong/noi lam viec-->

    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label">Nghề nghiệp</label>
      <div class="col-md-4">
        <div class="md-radio-inline">
          <div class="md-radio">
            <input type="radio" id="radio_renter_career1" name="job" class="md-radiobtn" value="1">
            <label for="radio_renter_career1">
              <span></span>
              <span class="check"></span>
              <span class="box"></span> Sinh viên </label>
          </div>
          <div class="md-radio">
            <input type="radio" id="radio_renter_career2" checked name="job" class="md-radiobtn" value="2">
            <label for="radio_renter_career2">
              <span></span>
              <span class="check"></span>
              <span class="box"></span> Người đi làm </label>
          </div>
        </div>
      </div>

      <label class="col-md-2 control-label">Nơi công tác</label>
      <div class="col-md-4">
        <input class="string optional bg-maxlength form-control" placeholder="Nhập trường học/nơi làm việc" maxlength="255" size="255" type="text" name="university" id="admin_renter_university">
        <div class="form-control-focus"></div>
      </div>
    </div>
   
    <hr>
    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label">Họ tên bố (mẹ)</label>
      <div class="col-md-4">
        <input class="string optional bg-maxlength form-control" placeholder="Nhập họ tên bố (mẹ)" maxlength="255" size="255" type="text" name="parent_name" id="admin_renter_parent_name">
        <div class="form-control-focus"></div>
      </div>

      <label class="col-md-2 control-label">SĐT liên hệ</label>
      <div class="col-md-4">
        <input class="string tel optional bg-maxlength form-control phone-number" placeholder="Nhập SĐT liên hệ" maxlength="50" type="tel" size="50" name="parent_phone" id="admin_renter_parent_phone">
        <div class="form-control-focus"></div>
      </div>
    </div>
    <!-- Anh CMND -->
    <hr>
    <div>
      <div class="form-group row form-md-line-input">
        @php 
          $ImageArray = [
            'avatar' => 'Ảnh Đại diện',
            'identity_card_font_image' => 'CMND mặt trước',
            'identity_card_behind_image' => 'CMND mặt sau'
          ]
        @endphp
        @foreach ($ImageArray as $key => $value)
        <div class="fileinput fileinput-new col-md-4 text-center" data-provides="fileinput">
          <p>{{$value}}</p>
          <div class="file-upload file-upload-{{$loop->index}}">
            <button class="file-upload-btn file-upload-btn-{{$loop->index}}" type="button" onclick="$('.file-upload-input-{{$loop->index}}').trigger( 'click' )">Thêm Ảnh</button>
            <div class="image-upload-wrap image-upload-wrap-{{$loop->index}}">
              <input class="file-upload-input file-upload-input-{{$loop->index}}" type="file" name="{{$key}}" onchange="readURL(this,{{$loop->index}});" accept="image/*" />
              <div class="drag-text">
                <h3>Kéo thả hoặc click vào để upload hình ảnh</h3>
              </div>
            </div>
            
          <div class="file-upload-content file-upload-content-{{$loop->index}}">
            <img class="file-upload-image file-upload-image-{{$loop->index}}" src="#" alt="your image" />
            <div class="image-title-wrap image-title-wrap-{{$loop->index}}">
          <button type="button" onclick="removeUpload({{$loop->index}})" class="remove-image">Xóa <span class="image-title image-title-{{$loop->index}}">Thêm hình ảnh</span></button>
        </div>
    </div>
  </div>
        </div>
        
        @endforeach

    </div>
    <div class="form-group row form-md-line-input">
      <label class="col-md-2 control-label">Ghi chú</label>
      <div class="col-md-10">
        <textarea class="text optional bg-maxlength form-control autosizeme" placeholder="Nhập ghi chú" maxlength="500" name="note" id="admin_renter_note"></textarea>
        <div class="form-control-focus"></div>
      </div>
    </div>

      <div class="clearfix margin-top-20"></div>
    </div>
  </div>

  <div class="form-actions margin-top-20">
    <div class="row">
      <div class="col-md-12 text-right">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
        <button name="button" type="submit" class="btn btn-default btnFormSubmit btn green">
            <i class="fa fa-floppy-o" aria-hidden="true"></i>Lưu lại
</button>        </div>
    </div>
  </div>
</form>
      </div>
       {{-- end modal-body --}}
     
    </div>
  </div>
</div>
<style>
  .md-radio-inline .md-radio {
    display: inline-block;
    margin-right: 10px;
  }

.file-upload {
  background-color: #ffffff;
  width: auto;
  margin: 0 auto;
  padding: 20px;
}

.file-upload-btn {
  width: 100%;
  margin: 0;
  color: #fff;
  background: #1FB264;
  border: none;
  padding: 10px;
  border-radius: 4px;
  border-bottom: 4px solid #15824B;
  transition: all .2s ease;
  outline: none;
  text-transform: uppercase;
  font-weight: 700;
}

.file-upload-btn:hover {
  background: #1AA059;
  color: #ffffff;
  transition: all .2s ease;
  cursor: pointer;
}

.file-upload-btn:active {
  border: 0;
  transition: all .2s ease;
}

.file-upload-content {
  display: none;
  text-align: center;
}

.file-upload-input {
  position: absolute;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
  outline: none;
  opacity: 0;
  cursor: pointer;
}

.image-upload-wrap {
  margin-top: 20px;
  border: 4px dashed #1FB264;
  position: relative;
}

.image-dropping,
.image-upload-wrap:hover {
  background-color: #1FB264;
  border: 4px dashed #ffffff;
}

.image-title-wrap {
  padding: 0 15px 15px 15px;
  color: #222;
}

.drag-text {
  text-align: center;
}

.drag-text h3 {
  font-weight: 100;
  text-transform: uppercase;
  color: #15824B;
  padding: 60px 0;
}

.file-upload-image {
  max-height: 200px;
  max-width: 100%;
  margin: auto;
  padding: 20px;
}

.remove-image {
  width: 100%;
  margin: 0;
  color: #fff;
  background: #cd4535;
  border: none;
  padding: 10px;
  border-radius: 4px;
  border-bottom: 4px solid #b02818;
  transition: all .2s ease;
  outline: none;
  text-transform: uppercase;
  font-weight: 700;
}

.remove-image:hover {
  background: #c13b2a;
  color: #ffffff;
  transition: all .2s ease;
  cursor: pointer;
}

.remove-image:active {
  border: 0;
  transition: all .2s ease;
}
</style>

<script>
  function readURL(input,index) {
  if (input.files && input.files[0]) {

    var reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap-'+index).hide();

      $('.file-upload-image-'+index).attr('src', e.target.result);
      $('.file-upload-content-'+index).show();

      $('.image-title-'+index).html(input.files[0].name);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload(index);
  }
}

function removeUpload(index) {
  $('.file-upload-input-'+index).replaceWith($('.file-upload-input-'+index).clone());
  $('.file-upload-content-'+index).hide();
  $('.image-upload-wrap-'+index).show();
}
// avatar
$('.image-upload-wrap-0').bind('dragover', function () {
		$('.image-upload-wrap-0').addClass('image-dropping-0');
	});

	$('.image-upload-wrap-0').bind('dragleave', function () {
		$('.image-upload-wrap-0').removeClass('image-dropping-0');
});
//id font
$('.image-upload-wrap-1').bind('dragover', function () {
		$('.image-upload-wrap-1').addClass('image-dropping-1');
	});

	$('.image-upload-wrap-1').bind('dragleave', function () {
		$('.image-upload-wrap-1').removeClass('image-dropping-1');
});
//id behind
$('.image-upload-wrap-2').bind('dragover', function () {
		$('.image-upload-wrap-2').addClass('image-dropping-2');
	});

	$('.image-upload-wrap-2').bind('dragleave', function () {
		$('.image-upload-wrap-2').removeClass('image-dropping-2');
});

</script>
