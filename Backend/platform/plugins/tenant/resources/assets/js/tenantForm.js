import Axios from 'axios';
import Common from '../../../../../../public/vendor/core/js/common';
const common = new Common;
const toastr = require('toastr');
window.axios = require('axios');

export default class tenantForm {
    // constructor() {
    //     this.timeout = timeout;
    // }

    prepareData() {
        let Model = {
            'id': '',
            'name': '',
            'email': '',
            'phone': '',
            'avatar': '',
            'cmnd_image': '',
            'cmnd_number': '',
            'cmnd_date_create': '',
            'cmnd_place': '',
            'birth_day': '',
            'parent_id': '',
            'room_forrent_id': '',
            'tenant_type': '',
            'province_id': '',
            'district_id': '',
            'ward_id': '',
            'family_record_book': '',
            'job': '',
            'work_place': '',
            'sex': '',
            'parent_name': '',
            'parent_phone': '',
            'note': '',
            'status': '',
        }

    }
    toQueryObject($form) {
        var serializedObj = {}
        var serialized = $form.serializeArray().map(function (e, i) {
            serializedObj[e.name] = e.value

        });
        return serializedObj;
    }

    validationForm(data, configs, validateSubmitInventory) {
        var doValidationInputForm = (data, configs) => {
            var errors = [];
            configs.forEach((conf, index) => {
                var appendValidation = true;
                if (conf.appendRules) {
                    appendValidation = conf.appendRules(data)
                }
                var inputName = conf['inputName']
                var messageKey = conf['messageKey'] || inputName
                var validation = !data[inputName] && appendValidation;
                if (validation) {
                    errors.push(messageKey)
                }
            })
            return errors
        }

        var getMessage = (errors) => {
            errors.forEach((messageKey) => {
                let messages = validateSubmitInventory;
                return toastr.warning(`<span class="remixicon-error-warning-line">${ messages[messageKey] }</span><span></span>`);

            })
        }

        var errors = doValidationInputForm(data, configs);
        if (errors.length > 0) {
            getMessage(errors);
            throw 'validation request';
        }
    }

    CreateOrUpdateTennant($form, url, type = null) {
        var data = this.toQueryObject($form);
        this.prepareData();
        const formData = new FormData();

        $.each($("input[type='file']"), function (i, file) {
            formData.append(file.name, file.files[0]);
        });
        $.each(data, function (key, value) {
            formData.append(key, value);
        });
        const configs = {
            headers: {
                'content-type': 'multipart/form-data'
            },
        }
        Axios.post(url,
                formData, configs
            ).then(response => {
                toastr.success(`<span class="remixicon-error-warning-line">${ response.message }</span><span></span>`);
            })
            .catch(error => {
                this.handleErro(error)
            });


    }

    handleErro(error) {
        let errorArray = error.response.data.errors;
        let erroMessageArr = [];
        Object.keys(errorArray).forEach(function (value, index) {
            erroMessageArr.push(errorArray[value]);

        });
        erroMessageArr.forEach(messageArr => {
            messageArr.forEach(message => {
                toastr.error(`<span class="remixicon-error-warning-line">${message}</span><span></span>`);
            })
        });

    }

}