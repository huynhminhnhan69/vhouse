import tenantForm from '../js/tenantForm';

var $form = $("#FormTenant");
const instanceTenantForm = new tenantForm;
$form.on("submit", function (e) {
    var route = $(this).data('route');
    instanceTenantForm.CreateOrUpdateTennant($form, route)
    e.preventDefault();
});