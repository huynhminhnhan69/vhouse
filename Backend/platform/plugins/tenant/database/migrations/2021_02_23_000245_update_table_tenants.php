<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTableTenants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('tenants', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 120);
            $table->string('email', 120);
            $table->integer('phone');
            $table->string('avatar', 255)->nullable();
            $table->string('cmnd_image', 500)->nullable();
            $table->integer('cmnd_number');
            $table->integer('cmnd_date_create');
            $table->string('cmnd_place', 500);
            $table->date('birth_day')->nullable();
            $table->uuid('parent_id')->nullable()->default(1);
            $table->uuid('room_forrent_id')->nullable();
            $table->string('tenant_type')->nullable(); // gd , vo chong , con cai
            $table->integer('province_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('ward_id')->nullable();
            $table->string('family_record_book', 500)->nullable();
            $table->string('job', 200)->nullable();
            $table->string('work_place', 500)->nullable();
            $table->integer('sex')->nullable();
            $table->string('parent_name', 200)->nullable();
            $table->integer('parent_phone')->nullable();
            $table->longText('note');
            $table->string('status', 60)->default('published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
