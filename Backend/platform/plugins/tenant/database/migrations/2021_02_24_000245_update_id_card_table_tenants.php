<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateIdCardTableTenants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('tenants', 'cmnd_image')) {

            Schema::table('tenants', function (Blueprint $table) {
                $table->dropColumn('cmnd_image');
                $table->dropColumn('cmnd_number');
                $table->dropColumn('cmnd_date_create');
                $table->dropColumn('cmnd_place');
            });
        }
        Schema::table('tenants', function (Blueprint $table) {
            $table->string('identity_card_font_image', 500)->nullable();
            $table->string('identity_card_behind_image', 500)->nullable();
            $table->integer('identity_card');
            $table->integer('date_of_issue');
            $table->string('issued_place', 500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
