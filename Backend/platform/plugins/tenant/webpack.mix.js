let mix = require('laravel-mix');

const publicPath = 'public/vendor/core/plugins/tenant';
const resourcePath = './platform/plugins/tenant';

mix
    .js(resourcePath + '/resources/assets/js/tenant.js', publicPath + '/js')
    .copy(publicPath + '/js/tenant.js', resourcePath + '/public/js')

    .copyDirectory(resourcePath + '/resources/assets/css', publicPath + '/css');