<?php

Route::group(['namespace' => 'Botble\Tenant\Http\Controllers', 'middleware' => 'web'], function () {

    Route::group(['prefix' => config('core.base.general.admin_dir'), 'middleware' => 'auth'], function () {

        Route::resource('tenants', 'TenantController', ['names' => 'tenant']);

        Route::group(['prefix' => 'tenants'], function () {

            Route::delete('items/destroy', [
                'as'         => 'tenant.deletes',
                'uses'       => 'TenantController@deletes',
                'permission' => 'tenant.destroy',
            ]);
        });
    });

});
