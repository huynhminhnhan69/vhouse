<?php

namespace Botble\Tenant\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;
use Botble\Base\Traits\UsesUuid;

class Tenant extends BaseModel
{
    use EnumCastable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    use UsesUuid;

    protected $table = 'tenants';

    public $incrementing = false;


    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'phone',
        'avatar',
        'identity_card',
        'date_of_issue',
        'issued_place',
        'identity_card_font_image',
        'identity_card_behind_image',
        'birth_day',
        'parent_id',
        'room_forrent_id',
        'tenant_type',
        'province_id',
        'district_id',
        'ward_id',
        'family_record_book',
        'job',
        'work_place',
        'sex',
        'parent_name',
        'parent_phone',
        'note',
        'status',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string
     */
    protected $screen = TENANT_MODULE_SCREEN_NAME;

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];
}
