<?php

namespace Botble\Tenant\Http\Requests;

use Botble\Base\Enums\BaseStatusEnum;
use Botble\Support\Http\Requests\Request;
use Illuminate\Validation\Rule;

class TenantRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'bail|required',
            'phone' => 'bail|required|numeric',
            'identity_card' => 'bail|required|numeric',
            'issued_place' => 'bail|required',
            'date_of_issue' => 'bail|required',

        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Trường TÊN không được bỏ trống',
            'phone.required' => 'Trường SDT không được bỏ trống',
            'phone.numeric' => 'Trường SDT phải là một số',
            'identity_card.required' => 'Trường Số CMND/CCCD không được bỏ trống',
            'identity_card.numeric' => 'Trường Số CMND/CCCD phải là một số',
            'issued_place.required' => 'Trường nơi cấp không được bỏ trống',
            'date_of_issue.required' => 'Trường Số ngày cấp không được bỏ trống',
        ];
    }
}
