<?php

namespace Botble\Tenant\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Tenant\Http\Requests\TenantRequest;
use Botble\Tenant\Repositories\Interfaces\TenantInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\Tenant\Tables\TenantTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Tenant\Forms\TenantForm;
use Botble\Base\Forms\FormBuilder;
use Botble\Tenant\DTO\TenantData;
use Botble\Tenant\Models\Tenant;
use RvMedia;
use Botble\Tenant\Services\AddTenantService;

class TenantController extends BaseController
{
    /**
     * @var TenantInterface
     */
    protected $tenantRepository;

    /**
     * @var AddTenantService
     */
    protected $_addTenantService;

    /**
     * TenantController constructor.
     * @param TenantInterface $tenantRepository
     */
    public function __construct(TenantInterface $tenantRepository, AddTenantService $addTenantService)
    {
        $this->tenantRepository = $tenantRepository;
        $this->_addTenantService = $addTenantService;
    }

    /**
     * Display all tenants
     * @param TenantTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(TenantTable $table)
    {

        page_title()->setTitle(trans('plugins/tenant::tenant.name'));

        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/tenant::tenant.create'));

        return $formBuilder->create(TenantForm::class)->renderForm();
    }

    /**
     * Insert new Tenant into database
     *
     * @param TenantRequest $request
     * @return BaseHttpResponse
     */
    public function store(TenantRequest $request, BaseHttpResponse $response)
    {
        try {

            $tenantData = new TenantData();
            $files = [
                'avatar' => $request->avatar ?? '',
                'identity_card_font_image' => $request->identity_card_font_image ?? '',
                'identity_card_behind_image' => $request->identity_card_behind_image ?? '',

            ];

            // addTenantService
            $tenantData->fill($request->input());
            $tenant = $this->_addTenantService->handle($tenantData, $files);

            event(new CreatedContentEvent(TENANT_MODULE_SCREEN_NAME, $request, $tenant));

            return $response
                ->setPreviousUrl(route('tenant.index'))
                ->setNextUrl(route('tenant.edit', $tenant->id))
                ->setMessage(trans('core/base::notices.create_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setMessage(trans('core/base::notices.error'))
                ->setCode(500)
                ->toApiResponse();
        }
    }

    /**
     * Show edit form
     *
     * @param $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $tenant = $this->tenantRepository->findOrFail($id);

        event(new BeforeEditContentEvent(TENANT_MODULE_SCREEN_NAME, $request, $tenant));

        page_title()->setTitle(trans('plugins/tenant::tenant.edit') . ' "' . $tenant->name . '"');

        return $formBuilder->create(TenantForm::class, ['model' => $tenant])->renderForm();
    }

    /**
     * @param $id
     * @param TenantRequest $request
     * @return BaseHttpResponse
     */
    public function update($id, TenantRequest $request, BaseHttpResponse $response)
    {
        $tenant = $this->tenantRepository->findOrFail($id);

        $tenant->fill($request->input());

        $this->tenantRepository->createOrUpdate($tenant);

        event(new UpdatedContentEvent(TENANT_MODULE_SCREEN_NAME, $request, $tenant));

        return $response
            ->setPreviousUrl(route('tenant.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $tenant = $this->tenantRepository->findOrFail($id);

            $this->tenantRepository->delete($tenant);

            event(new DeletedContentEvent(TENANT_MODULE_SCREEN_NAME, $request, $tenant));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $tenant = $this->tenantRepository->findOrFail($id);
            $this->tenantRepository->delete($tenant);
            event(new DeletedContentEvent(TENANT_MODULE_SCREEN_NAME, $request, $tenant));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }
}
