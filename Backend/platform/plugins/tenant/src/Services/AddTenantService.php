<?php

namespace Botble\Tenant\Services;

use Botble\Tenant\Repositories\Interfaces\TenantInterface;
use Botble\Tenant\DTO\TenantData;
use Botble\Tenant\Models\Tenant;
use RvMedia;
use Botble\Tenant\Enums\TennantStatusEnum;
use Exception;
use Illuminate\Support\Carbon;

class AddTenantService
{
    /**
     * @var TenantInterface
     */
    protected $_tenantrepository;



    public function __construct()
    {

        $this->_tenantrepository = app(TenantInterface::class);
    }

    /**
     * handle
     *
     * @param  TenantData $tenantData
     * @param  mixed $identityFiles
     * @return Tenant
     */
    public function handle(TenantData $tenantData, $identityFiles)
    {
        try {
            \DB::beginTransaction();
            foreach ($identityFiles as $key => $identityFile) {
                $resurt = RvMedia::handleUpload($identityFile, 0);
                if ($resurt['error'] != true) {
                    $tenantData->$key = $resurt['data']->url;
                }
            }
            $tenantData->status = TennantStatusEnum::OUTSIDE;
            $tenantData->date_of_issue =  date('Y-m-d', strtotime(str_replace('/', '-', $tenantData->date_of_issue)));
            $tenantData->birth_day =  date('Y-m-d', strtotime(str_replace('/', '-', $tenantData->birth_day)));
            $tenant = new Tenant();
            $tenant->fill($tenantData->toArray());
            $tenant = $this->_tenantrepository->createOrUpdate($tenant);
            \DB::commit();
            return $tenant;
        } catch (Exception $e) {
            \DB::rollBack();
            dd($e);
            throw new Exception($e->getMessage());
        }
    }
}
