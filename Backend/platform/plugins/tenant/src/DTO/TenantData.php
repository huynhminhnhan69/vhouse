<?php

namespace Botble\Tenant\DTO;

use Botble\Tenant\Repositories\Interfaces\TenantInterface;
use Illuminate\Support\Facades\Date;

class TenantData
{
    public  $name;
    public  $email;
    public  $phone;
    public  $identity_card;
    public  $date_of_issue;
    public  $issued_place;
    public  $identity_card_font_image;
    public  $identity_card_behind_image;
    public  $birth_day;
    public  $parent_id;
    public  $room_forrent_id;
    public  $tenant_type;
    public  $province_id;
    public  $district_id;
    public  $ward_id;
    public  $family_record_book;
    public  $job;
    public  $work_place;
    public  $sex;
    public  $parent_name;
    public  $parent_phone;
    public  $note;
    public  $status;

    /**
     * fill
     *
     * @param  array $arrData
     * @return void
     */
    public function fill($arrData)
    {
        $vars = get_object_vars($this);
        foreach ($vars as $property => $oldValue) {
            $this->$property = isset($arrData[$property]) ? $arrData[$property] : NULL;
        }
    }

    /**
     * toArray
     *
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}
