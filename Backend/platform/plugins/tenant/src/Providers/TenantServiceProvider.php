<?php

namespace Botble\Tenant\Providers;

use Botble\Tenant\Models\Tenant;
use Illuminate\Support\ServiceProvider;
use Botble\Tenant\Repositories\Caches\TenantCacheDecorator;
use Botble\Tenant\Repositories\Eloquent\TenantRepository;
use Botble\Tenant\Repositories\Interfaces\TenantInterface;
use Botble\Base\Supports\Helper;
use Event;
use Botble\Base\Traits\LoadAndPublishDataTrait;
use Illuminate\Routing\Events\RouteMatched;

class TenantServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function register()
    {
        $this->app->bind(TenantInterface::class, function () {
            return new TenantCacheDecorator(new TenantRepository(new Tenant));
        });

        Helper::autoload(__DIR__ . '/../../helpers');
    }

    public function boot()
    {
        $this->setNamespace('plugins/tenant')
            ->loadAndPublishConfigurations(['permissions'])
            ->loadMigrations()
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadRoutes(['web']);

        Event::listen(RouteMatched::class, function () {
            dashboard_menu()->registerItem([
                'id'          => 'cms-plugins-tenant',
                'priority'    => 5,
                'parent_id'   => null,
                'name'        => 'plugins/tenant::tenant.name',
                'icon'        => 'fa fa-list',
                'url'         => route('tenant.index'),
                'permissions' => ['tenant.index'],
            ]);
        });
    }
}
