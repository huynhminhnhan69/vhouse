<?php

namespace Botble\Tenant\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Tenant\Repositories\Interfaces\TenantInterface;

class TenantCacheDecorator extends CacheAbstractDecorator implements TenantInterface
{

}
