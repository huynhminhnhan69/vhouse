<?php

namespace Botble\Tenant\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Tenant\Repositories\Interfaces\TenantInterface;

class TenantRepository extends RepositoriesAbstract implements TenantInterface
{
    /**
     * @var string
     */
    protected $screen = TENANT_MODULE_SCREEN_NAME;
}
