<?php

namespace Botble\Tenant\Enums;

use Botble\Base\Supports\Enum;
use Html;

/**
 * @method static BaseStatusEnum DRAFT()
 * @method static BaseStatusEnum PUBLISHED()
 * @method static BaseStatusEnum PENDING()
 */
class TennantStatusEnum extends Enum
{
    public const OUTSIDE = 'OUTSIDE';
    public const INSIDE = 'INSIDE';
    public const RETURN = 'RETURN';


    /**
     * @return string
     */
    public function toHtml()
    {
        switch ($this->value) {
            case self::OUTSIDE:
                return Html::tag('span', self::DRAFT()->label(), ['class' => 'label-info status-label'])
                    ->toHtml();
            case self::INSIDE:
                return Html::tag('span', self::PENDING()->label(), ['class' => 'label-warning status-label'])
                    ->toHtml();
            case self::RETURN:
                return Html::tag('span', self::PUBLISHED()->label(), ['class' => 'label-success status-label'])
                    ->toHtml();
            default:
                return null;
        }
    }
}
