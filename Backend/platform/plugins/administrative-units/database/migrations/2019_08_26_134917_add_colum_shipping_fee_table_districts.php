<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumShippingFeeTableDistricts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::table('districts', function (Blueprint $table){
            $table->decimal("shipping_fee", 8)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('districts', "shipping_fee")){
            Schema::table('districts', function (Blueprint $table){
                $table->dropColumn("shipping_fee");
            });
        }
    }
}
