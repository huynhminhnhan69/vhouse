<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdministrativeUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('slug', 120);
            $table->string('type', 120);
            $table->string('name_with_type', 255);
            $table->string('code', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });
        Schema::create('districts', function (Blueprint $table){
            $table->increments('id');
            $table->string('name', 120);
            $table->string('slug', 120);
            $table->string('type', 120);
            $table->string('name_with_type', 255);
            $table->string('path', 255);
            $table->string('path_with_type', 500);
            $table->string('parent_code', 120);
            $table->integer('parent_id');
            $table->string('code', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('wards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('slug', 120);
            $table->string('type', 120);
            $table->string('name_with_type', 255);
            $table->string('path', 255);
            $table->string('path_with_type', 500);
            $table->string('parent_code', 120);
            $table->integer('parent_id');
            $table->string('code', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
        Schema::dropIfExists('districts');
        Schema::dropIfExists('wards');
    }
}
