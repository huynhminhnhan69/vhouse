<?php
namespace Botble\AdministrativeUnits\Repositories\Caches;

use Botble\AdministrativeUnits\Repositories\Interfaces\WardInterface;
use Botble\Support\Repositories\Caches\CacheAbstractDecorator;

class WardCacheDecorator extends CacheAbstractDecorator implements WardInterface 
{
	public function getWardByParentId($parentId = 0){
		return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
	}	
}
