<?php
namespace Botble\AdministrativeUnits\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\AdministrativeUnits\Repositories\Interfaces\ProvinceInterface;

class ProvinceCacheDecorator extends CacheAbstractDecorator implements ProvinceInterface
{
	public function getAllProvince() {
		return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
	}
}
