<?php
namespace Botble\AdministrativeUnits\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\AdministrativeUnits\Repositories\Interfaces\DistrictInterface;
class DistrictCacheDecorator extends CacheAbstractDecorator implements DistrictInterface
{
	public function getDisctrictByParentId($parentId = 0){
		return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
	}
}
