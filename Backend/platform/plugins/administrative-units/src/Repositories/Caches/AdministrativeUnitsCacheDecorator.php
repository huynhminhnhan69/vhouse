<?php

namespace Botble\AdministrativeUnits\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\AdministrativeUnits\Repositories\Interfaces\AdministrativeUnitsInterface;

class AdministrativeUnitsCacheDecorator extends CacheAbstractDecorator implements AdministrativeUnitsInterface
{

}
