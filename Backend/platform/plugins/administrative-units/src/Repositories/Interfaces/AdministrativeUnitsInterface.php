<?php

namespace Botble\AdministrativeUnits\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;

interface AdministrativeUnitsInterface extends RepositoryInterface
{
}
