<?php
namespace Botble\AdministrativeUnits\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;

interface WardInterface extends RepositoryInterface
{
	public function getWardByParentId($parentId = 0);
}
