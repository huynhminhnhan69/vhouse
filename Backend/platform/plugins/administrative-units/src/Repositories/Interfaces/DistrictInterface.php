<?php
namespace Botble\AdministrativeUnits\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;
interface DistrictInterface extends RepositoryInterface
{
	public function getDisctrictByParentId($parentId = 0);
}
