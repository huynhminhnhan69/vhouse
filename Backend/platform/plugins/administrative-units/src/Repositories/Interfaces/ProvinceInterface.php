<?php
namespace Botble\AdministrativeUnits\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;

interface ProvinceInterface extends RepositoryInterface
{
	public function getAllProvince();
}
