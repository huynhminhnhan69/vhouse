<?php
namespace Botble\AdministrativeUnits\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\AdministrativeUnits\Repositories\Interfaces\ProvinceInterface;

class ProvinceRepository extends RepositoriesAbstract implements ProvinceInterface
{
    /**
     * @var string
     */
    protected $screen = ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME;

    public function getAllProvince() {
        $data = $this->model->select('id','name')->orderBy('name');
        return $data->get();
    }    
}
