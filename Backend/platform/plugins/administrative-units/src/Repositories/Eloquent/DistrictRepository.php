<?php
namespace Botble\AdministrativeUnits\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\AdministrativeUnits\Repositories\Interfaces\DistrictInterface;

class DistrictRepository extends RepositoriesAbstract implements DistrictInterface
{
    /**
     * @var string
     */
    protected $screen = ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME;

    public function getDisctrictByParentId($parentId = 0){
        $data = $this->model->select('id','name')->orderByRaw('CAST(`name` as UNSIGNED)');
        if ($parentId){
            $data = $data->where('parent_id', '=', $parentId);
        }
        return $data->get();
    }
}
