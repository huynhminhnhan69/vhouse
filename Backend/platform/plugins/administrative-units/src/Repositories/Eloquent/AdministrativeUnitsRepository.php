<?php

namespace Botble\AdministrativeUnits\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\AdministrativeUnits\Repositories\Interfaces\AdministrativeUnitsInterface;

class AdministrativeUnitsRepository extends RepositoriesAbstract implements AdministrativeUnitsInterface
{
    /**
     * @var string
     */
    protected $screen = ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME;
}
