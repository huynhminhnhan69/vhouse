<?php

namespace Botble\AdministrativeUnits\Http\Controllers;

use Botble\AdministrativeUnits\Repositories\Interfaces\DistrictInterface;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Events\BeforeEditContentEvent;
use Botble\AdministrativeUnits\Http\Requests\AdministrativeUnitsRequest;
use Botble\AdministrativeUnits\Repositories\Interfaces\AdministrativeUnitsInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\AdministrativeUnits\Tables\AdministrativeUnitsTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\AdministrativeUnits\Forms\AdministrativeUnitsForm;
use Botble\Base\Forms\FormBuilder;

use Botble\AdministrativeUnits\Repositories\Interfaces\ProvinceInterface;

class AdministrativeUnitsController extends BaseController
{
    /**
     * @var AdministrativeUnitsInterface
     */
    protected $administrativeUnitsRepository;

    /**
     * @var ProvinceInterface
     */
    protected $provinceInteface;

    /**
     * @var DistrictInterface
     */
    protected $districtInterface;

    /**
     * AdministrativeUnitsController constructor.
     * @param AdministrativeUnitsInterface $administrativeUnitsRepository
     */
    public function __construct(AdministrativeUnitsInterface $administrativeUnitsRepository, ProvinceInterface $provinceInteface, DistrictInterface $districtInterface)
    {
        $this->administrativeUnitsRepository = $administrativeUnitsRepository;

        $this->provinceInteface = $provinceInteface;

        $this->districtInterface = $districtInterface;
    }

    /**
     * Display all administrative_units
     * @param AdministrativeUnitsTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(AdministrativeUnitsTable $table)
    {
        page_title()->setTitle(trans('plugins/administrative-units::administrative-units.name'));

        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/administrative-units::administrative-units.create'));

        return $formBuilder->create(AdministrativeUnitsForm::class)->renderForm();
    }

    /**
     * Insert new AdministrativeUnits into database
     *
     * @param AdministrativeUnitsRequest $request
     * @return BaseHttpResponse
     */
    public function store(AdministrativeUnitsRequest $request, BaseHttpResponse $response)
    {
        $administrative_units = $this->administrativeUnitsRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME, $request, $administrative_units));

        return $response
            ->setPreviousUrl(route('administrative_units.index'))
            ->setNextUrl(route('administrative_units.edit', $administrative_units->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * Show edit form
     *
     * @param $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $administrative_units = $this->administrativeUnitsRepository->findOrFail($id);

        event(new BeforeEditContentEvent(ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME, $request, $administrative_units));

        page_title()->setTitle(trans('plugins/administrative-units::administrative-units.edit') . ' "' . $administrative_units->name . '"');

        return $formBuilder->create(AdministrativeUnitsForm::class, ['model' => $administrative_units])->renderForm();
    }

    /**
     * @param $id
     * @param AdministrativeUnitsRequest $request
     * @return BaseHttpResponse
     */
    public function update($id, AdministrativeUnitsRequest $request, BaseHttpResponse $response)
    {
        $administrative_units = $this->administrativeUnitsRepository->findOrFail($id);

        $administrative_units->fill($request->input());

        $this->administrativeUnitsRepository->createOrUpdate($administrative_units);

        event(new UpdatedContentEvent(ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME, $request, $administrative_units));

        return $response
            ->setPreviousUrl(route('administrative_units.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $administrative_units = $this->administrativeUnitsRepository->findOrFail($id);

            $this->administrativeUnitsRepository->delete($administrative_units);

            event(new DeletedContentEvent(ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME, $request, $administrative_units));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $administrative_units = $this->administrativeUnitsRepository->findOrFail($id);
            $this->administrativeUnitsRepository->delete($administrative_units);
            event(new DeletedContentEvent(ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME, $request, $administrative_units));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function getData(Request $request, BaseHttpResponse $response){
        $attribute = $request->get('attribute');
        $type      = $request->get('type', null);
        $data = $this->provinceInteface->findById($attribute) ?? $this->provinceInteface->getFirstBy(['name' => $attribute]);
        if($type && $type == "district")
            $data = $this->districtInterface->findById($attribute) ?? $this->districtInterface->getFirstBy(['name' => $attribute]);
        if(!$data) return $response
            ->setError()
            ->setMessage(trans('plugins/administrative-units::administrative-units.notices.cannot_find'));
        return $response->setData( $type && $type == "district" ? $data->wards : $data->districts)->toApiResponse();
    }

}
