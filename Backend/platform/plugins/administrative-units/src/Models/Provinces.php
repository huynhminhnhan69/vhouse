<?php

namespace Botble\AdministrativeUnits\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;

class Provinces extends BaseModel
{
    use EnumCastable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provinces';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'name_with_type',
        'code',
        'status'
    ];

    /**
     * @return mixed
     */
    protected function districts(){
        return $this->hasMany(Districts::class, 'parent_id');
    }

    /**
     * @var string
     */
    protected $screen = ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME;

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];
}
