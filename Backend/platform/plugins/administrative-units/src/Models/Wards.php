<?php

namespace Botble\AdministrativeUnits\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;

class Wards extends BaseModel
{
    use EnumCastable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wards';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'name_with_type',
        'path',
        'path_with_type',
        'parent_code',
        'parent_id',
        'code',
        'status'
    ];

    /**
     * @var string
     */
    protected $screen = ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME;

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];
}
