<?php

namespace Botble\AdministrativeUnits\Tables;

use Auth;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\AdministrativeUnits\Repositories\Interfaces\AdministrativeUnitsInterface;
use Botble\Table\Abstracts\TableAbstract;
use Illuminate\Contracts\Routing\UrlGenerator;
use Yajra\DataTables\DataTables;

class AdministrativeUnitsTable extends TableAbstract
{

    /**
     * @var bool
     */
    protected $hasActions = true;

    /**
     * @var bool
     */
    protected $hasFilter = true;

    /**
     * AdministrativeUnitsTable constructor.
     * @param DataTables $table
     * @param UrlGenerator $urlDevTool
     * @param AdministrativeUnitsInterface $administrativeUnitsRepository
     */
    public function __construct(DataTables $table, UrlGenerator $urlDevTool, AdministrativeUnitsInterface $administrativeUnitsRepository)
    {
        $this->repository = $administrativeUnitsRepository;
        $this->setOption('id', 'table-plugins-administrative_units');
        parent::__construct($table, $urlDevTool);

        if (!Auth::user()->hasAnyPermission(['administrative_units.edit', 'administrative_units.destroy'])) {
            $this->hasOperations = false;
            $this->hasActions = false;
        }
    }

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     * @since 2.1
     */
    public function ajax()
    {
        $data = $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                if (!Auth::user()->hasPermission('administrative_units.edit')) {
                    return $item->name;
                }
                return anchor_link(route('administrative_units.edit', $item->id), $item->name);
            })
            ->editColumn('checkbox', function ($item) {
                return table_checkbox($item->id);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at, config('core.base.general.date_format.date'));
            })
            ->editColumn('status', function ($item) {
                return $item->status->toHtml();
            });

        return apply_filters(BASE_FILTER_GET_LIST_DATA, $data, ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME)
            ->addColumn('operations', function ($item) {
                return table_actions('administrative_units.edit', 'administrative_units.destroy', $item);
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Get the query object to be processed by table.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     * @since 2.1
     */
    public function query()
    {
        $model = $this->repository->getModel();
        $query = $model->select([
            'administrative_units.id',
            'administrative_units.name',
            'administrative_units.created_at',
            'administrative_units.status',
        ]);

        return $this->applyScopes(apply_filters(BASE_FILTER_TABLE_QUERY, $query, $model, ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME));
    }

    /**
     * @return array
     * @since 2.1
     */
    public function columns()
    {
        return [
            'id' => [
                'name' => 'administrative_units.id',
                'title' => trans('core/base::tables.id'),
                'width' => '20px',
            ],
            'name' => [
                'name' => 'administrative_units.name',
                'title' => trans('core/base::tables.name'),
                'class' => 'text-left',
            ],
            'created_at' => [
                'name' => 'administrative_units.created_at',
                'title' => trans('core/base::tables.created_at'),
                'width' => '100px',
            ],
            'status' => [
                'name' => 'administrative_units.status',
                'title' => trans('core/base::tables.status'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * @return array
     * @since 2.1
     * @throws \Throwable
     */
    public function buttons()
    {
        $buttons = $this->addCreateButton(route('administrative_units.create'), 'administrative_units.create');

        return apply_filters(BASE_FILTER_TABLE_BUTTONS, $buttons, ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME);
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function bulkActions(): array
    {
        return $this->addDeleteAction(route('administrative_units.deletes'), 'administrative_units.destroy', parent::bulkActions());
    }

    /**
     * @return array
     */
    public function getBulkChanges(): array
    {
        return [
            'administrative_units.name' => [
                'title'    => trans('core/base::tables.name'),
                'type'     => 'text',
                'validate' => 'required|max:120',
                'callback' => 'getNames',
            ],
            'administrative_units.status' => [
                'title'    => trans('core/base::tables.status'),
                'type'     => 'select',
                'choices'  => BaseStatusEnum::labels(),
                'validate' => 'required|in:' . implode(',', BaseStatusEnum::values()),
            ],
            'administrative_units.created_at' => [
                'title' => trans('core/base::tables.created_at'),
                'type'  => 'date',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getNames()
    {
        return $this->repository->pluck('administrative_units.name', 'administrative_units.id');
    }
}
