<?php

namespace Botble\AdministrativeUnits\Providers;

use Botble\AdministrativeUnits\Models\AdministrativeUnits;
use Botble\AdministrativeUnits\Models\Districts;
use Botble\AdministrativeUnits\Models\Provinces;
use Botble\AdministrativeUnits\Models\Wards;
use Botble\AdministrativeUnits\Repositories\Caches\DistrictCacheDecorator;
use Botble\AdministrativeUnits\Repositories\Caches\ProvinceCacheDecorator;
use Botble\AdministrativeUnits\Repositories\Caches\WardCacheDecorator;
use Botble\AdministrativeUnits\Repositories\Eloquent\DistrictRepository;
use Botble\AdministrativeUnits\Repositories\Eloquent\ProvinceRepository;
use Botble\AdministrativeUnits\Repositories\Eloquent\WardRepository;
use Botble\AdministrativeUnits\Repositories\Interfaces\DistrictInterface;
use Botble\AdministrativeUnits\Repositories\Interfaces\ProvinceInterface;
use Botble\AdministrativeUnits\Repositories\Interfaces\WardInterface;
use Illuminate\Support\ServiceProvider;
use Botble\AdministrativeUnits\Repositories\Caches\AdministrativeUnitsCacheDecorator;
use Botble\AdministrativeUnits\Repositories\Eloquent\AdministrativeUnitsRepository;
use Botble\AdministrativeUnits\Repositories\Interfaces\AdministrativeUnitsInterface;
use Botble\Base\Supports\Helper;
use Event;
use Botble\Base\Traits\LoadAndPublishDataTrait;
use Illuminate\Routing\Events\RouteMatched;
use Botble\AdministrativeUnits\Providers\CommandServiceProvider;

class AdministrativeUnitsServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function register()
    {
        $this->app->bind(AdministrativeUnitsInterface::class, function () {
            return new AdministrativeUnitsCacheDecorator(new AdministrativeUnitsRepository(new AdministrativeUnits));
        });

        $this->app->bind(ProvinceInterface::class, function () {
            return new ProvinceCacheDecorator(new ProvinceRepository(new Provinces()));
        });

        $this->app->bind(DistrictInterface::class, function () {
            return new DistrictCacheDecorator(new DistrictRepository(new Districts()));
        });

        $this->app->bind(WardInterface::class, function () {
            return new WardCacheDecorator(new WardRepository(new Wards()));
        });


        Helper::autoload(__DIR__ . '/../../helpers');
    }

    public function boot()
    {
        $this->setNamespace('plugins/administrative-units')
            ->loadAndPublishConfigurations(['permissions'])
            ->loadMigrations()
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadRoutes(['web']);

            Event::listen(RouteMatched::class, function () {
//                dashboard_menu()->registerItem([
//                    'id'          => 'cms-plugins-administrative_units',
//                    'priority'    => 5,
//                    'parent_id'   => null,
//                    'name'        => 'plugins/administrative-units::administrative-units.name',
//                    'icon'        => 'fa fa-list',
//                    'url'         => route('administrative_units.index'),
//                    'permissions' => ['administrative_units.index'],
//                ]);
            });
        $this->app->register(CommandServiceProvider::class);
    }
}
