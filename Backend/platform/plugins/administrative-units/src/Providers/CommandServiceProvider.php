<?php

namespace Botble\AdministrativeUnits\Providers;

use Botble\AdministrativeUnits\Commands\CreateAdministrativeUnitDataCommand;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateAdministrativeUnitDataCommand::class,
            ]);
        }
    }
}
