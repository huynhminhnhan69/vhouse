<?php

namespace Botble\AdministrativeUnits\Commands;

use Botble\AdministrativeUnits\Repositories\Interfaces\DistrictInterface;
use Botble\AdministrativeUnits\Repositories\Interfaces\ProvinceInterface;
use Botble\AdministrativeUnits\Repositories\Interfaces\WardInterface;
use Illuminate\Console\Command;
use Botble\AdministrativeUnits\Http\Requests\DisctrictRequest;
use Botble\AdministrativeUnits\Http\Requests\ProvinceRequest;
use Botble\Base\Events\CreatedContentEvent;

class CreateAdministrativeUnitDataCommand extends Command
{

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'cms:administrative-unit:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create data for administrative units';

    /**
     * @var mixed
     */
    protected $districtRepository;

    /**
     * @var MenuNodeInterface
     */
    protected $provineRepository;

    /**
     * @var MenuLocationInterface
     */
    protected $wardRepository;

    /**
     * Install constructor.
     * @param UserInterface $userRepository
     *
     */
    public function __construct( DistrictInterface $districtRepository, ProvinceInterface $provineRepository, WardInterface $wardRepository)
    {
        parent::__construct();

        $this->districtRepository = $districtRepository;
        $this->provineRepository = $provineRepository;
        $this->wardRepository = $wardRepository;
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {
        $this->createData();
    }

    /**
     * Create a superuser.
     *
     * @return bool
     *
     */
    protected function createData()
    {
        $this->info('Creating Data...');

        try {
            $path = resource_path() . "/documents/vietnam-provinces.json"; 

            $json = json_decode(file_get_contents($path), true);
            //create data
            $this->provineRepository->getModel()->truncate();
            $this->districtRepository->getModel()->truncate();
            $this->wardRepository->getModel()->truncate();

            $this->createNode($json);


            $this->info('Data is created.');
        } catch (Exception $exception) {
            $this->error('Data could not be created.');
            $this->error($exception->getMessage());
        }

        return true;
    }

    function createNode($array, $parent_id = 0 , $type = 'thanh-pho'){
        foreach ($array as $key => $element){
            switch ($type) {
                case 'quan-huyen':
                    $node = $this->districtRepository->getModel();
                    $child = 'xa-phuong';
                    break;
                case 'xa-phuong':
                    $node = $this->wardRepository->getModel();
                    break;
                default:
                    $node = $this->provineRepository->getModel();
                    $child = 'quan-huyen';
                    break;
            }
            $node->name = $element['name'];
            $node->slug = $element['slug'];
            $node->type = $element['type'];
            $node->name_with_type = $element['name_with_type'];
            $node->code = $element['code'];
            $node->status = 'published';

            if($parent_id){
                $node->path = $element['path'];
                $node->path_with_type = $element['path_with_type'];
                $node->parent_code = $element['parent_code'];
                $node->parent_id = $parent_id;
            }

            $element['language'] = 'vi';
            switch ($type) {
                case 'quan-huyen':
                    $node = $this->districtRepository->createOrUpdate($node);
                    $child = 'xa-phuong';
                    break;
                case 'xa-phuong':
                    $node = $this->wardRepository->createOrUpdate($node);
                    break;
                default:
                    $node = $this->provineRepository->createOrUpdate($node);
                    $child = 'quan-huyen';
                    break;
            }
            $distRequest = new ProvinceRequest($element);
            event(new CreatedContentEvent(ADMINISTRATIVE_UNITS_MODULE_SCREEN_NAME, $distRequest, $node));
            if (isset($child) && count($element[$child])> 0){
                $this->createNode($element[$child], $node->id, $child);
            }
        }
    }
    function stripVN($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);

        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        return $str;
    }
}
