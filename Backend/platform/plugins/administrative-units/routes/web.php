<?php

Route::group(['namespace' => 'Botble\AdministrativeUnits\Http\Controllers', 'middleware' => 'web'], function () {

    Route::group(['prefix' => config('core.base.general.admin_dir'), 'middleware' => 'auth'], function () {

        Route::resource('administrative-units', 'AdministrativeUnitsController', ['names' => 'administrative_units']);

        Route::group(['prefix' => 'administrative-units'], function () {

            Route::delete('items/destroy', [
                'as'         => 'administrative_units.deletes',
                'uses'       => 'AdministrativeUnitsController@deletes',
                'permission' => 'administrative_units.destroy',
            ]);


        });
    });

    if (defined('THEME_MODULE_SCREEN_NAME')) {
        Route::group(apply_filters(BASE_FILTER_GROUP_PUBLIC_ROUTE, []), function () {
            Route::group(['prefix' => 'administrative-units'], function () {
                Route::get('get-district',[
                    'as'         => 'administrative_units.get-data',
                    'uses'       => 'AdministrativeUnitsController@getData'
                ]);
            });
        });
    };

});
