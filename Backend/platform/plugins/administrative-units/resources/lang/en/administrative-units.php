<?php

return [
    'name' => 'AdministrativeUnits',
    'create' => 'New administrative_units',
    'edit' => 'Edit administrative_units',
    'notices'   => [
        'cannot_find'   => 'There are no record'
    ],
    "province/city"     => "Province/City",
    "district"          => "District",
    "ward"              => "Ward",
    "choose_province/cities"  => "Choose province or city",
    "choose_wards"      => "Choose wards",
    "choose_district"   => "Choose district"

];
