<?php

return [
    'name' => 'Đợn vị hành chính',
    'create' => 'Thêm đơn vị hành chính',
    'edit' => 'Chỉnh sửa đơn vị hành chính',
    "province/city"     => "Tỉnh/Thành phố",
    "district"          => "Quận/Huyện",
    "ward"              => "Phường/Xã",
    "choose_province/cities"  => "Chọn tỉnh/thành phố",
    "choose_wards"      => "Chọn xã/phường",
    "choose_district"   => "Chọn quận/huyện"
];
