<?php

return [
    [
        'name' => 'AdministrativeUnits',
        'flag' => 'administrative_units.index',
    ],
    [
        'name' => 'Create',
        'flag' => 'administrative_units.create',
        'parent_flag' => 'administrative_units.index',
    ],
    [
        'name' => 'Edit',
        'flag' => 'administrative_units.edit',
        'parent_flag' => 'administrative_units.index',
    ],
    [
        'name' => 'Delete',
        'flag' => 'administrative_units.destroy',
        'parent_flag' => 'administrative_units.index',
    ],
];