<?php

return [
    [
        'name' => 'House',
        'flag' => 'house.index',
    ],
    [
        'name' => 'Create',
        'flag' => 'house.create',
        'parent_flag' => 'house.index',
    ],
    [
        'name' => 'Edit',
        'flag' => 'house.edit',
        'parent_flag' => 'house.index',
    ],
    [
        'name' => 'Delete',
        'flag' => 'house.destroy',
        'parent_flag' => 'house.index',
    ],
];