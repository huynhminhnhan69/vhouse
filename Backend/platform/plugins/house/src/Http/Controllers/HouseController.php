<?php

namespace Botble\House\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\House\Http\Requests\HouseRequest;
use Botble\House\Repositories\Interfaces\HouseInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\House\Tables\HouseTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\House\Forms\HouseForm;
use Botble\Base\Forms\FormBuilder;

class HouseController extends BaseController
{
    /**
     * @var HouseInterface
     */
    protected $houseRepository;

    /**
     * HouseController constructor.
     * @param HouseInterface $houseRepository
     */
    public function __construct(HouseInterface $houseRepository)
    {
        $this->houseRepository = $houseRepository;
    }

    /**
     * Display all houses
     * @param HouseTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(HouseTable $table)
    {

        page_title()->setTitle(trans('plugins/house::house.name'));

        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/house::house.create'));

        return $formBuilder->create(HouseForm::class)->renderForm();
    }

    /**
     * Insert new House into database
     *
     * @param HouseRequest $request
     * @return BaseHttpResponse
     */
    public function store(HouseRequest $request, BaseHttpResponse $response)
    {
        $house = $this->houseRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(HOUSE_MODULE_SCREEN_NAME, $request, $house));

        return $response
            ->setPreviousUrl(route('house.index'))
            ->setNextUrl(route('house.edit', $house->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * Show edit form
     *
     * @param $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $house = $this->houseRepository->findOrFail($id);

        event(new BeforeEditContentEvent(HOUSE_MODULE_SCREEN_NAME, $request, $house));

        page_title()->setTitle(trans('plugins/house::house.edit') . ' "' . $house->name . '"');

        return $formBuilder->create(HouseForm::class, ['model' => $house])->renderForm();
    }

    /**
     * @param $id
     * @param HouseRequest $request
     * @return BaseHttpResponse
     */
    public function update($id, HouseRequest $request, BaseHttpResponse $response)
    {
        $house = $this->houseRepository->findOrFail($id);

        $house->fill($request->input());

        $this->houseRepository->createOrUpdate($house);

        event(new UpdatedContentEvent(HOUSE_MODULE_SCREEN_NAME, $request, $house));

        return $response
            ->setPreviousUrl(route('house.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $house = $this->houseRepository->findOrFail($id);

            $this->houseRepository->delete($house);

            event(new DeletedContentEvent(HOUSE_MODULE_SCREEN_NAME, $request, $house));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $house = $this->houseRepository->findOrFail($id);
            $this->houseRepository->delete($house);
            event(new DeletedContentEvent(HOUSE_MODULE_SCREEN_NAME, $request, $house));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }
}
