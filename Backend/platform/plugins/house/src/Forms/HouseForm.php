<?php

namespace Botble\House\Forms;

use Botble\Base\Forms\FormAbstract;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\House\Http\Requests\HouseRequest;

class HouseForm extends FormAbstract
{

    /**
     * @return mixed|void
     * @throws \Throwable
     */
    public function buildForm()
    {

        // 'id',
        // 'name',
        // 'address',
        // 'province_id',
        // 'district_id',
        // 'ward_id',
        // 'status'
        $this
            ->setModuleName(HOUSE_MODULE_SCREEN_NAME)
            ->setValidatorClass(HouseRequest::class)
            ->withCustomFields()
            ->add('name', 'text', [
                'label' => trans('core/base::forms.name'),
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => trans('core/base::forms.name_placeholder'),
                    'data-counter' => 120,
                ],
            ])
            ->add('address', 'text', [
                'label' => 'Dia chi',
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => 'Dia chi',
                    'data-counter' => 120,
                ],
            ])
            ->add('province_id', 'customSelect', [
                'label' => 'Tinh',
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => 'Chon Tinh/TP',
                    'class' => 'form-control select-full',
                ],
                'choices'    => [1, 2, 3]
            ])
            ->add('district_id', 'customSelect', [
                'label' => 'Quan/Huyen',
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => 'Chon Quan/Huyen',
                    'class' => 'form-control select-full',
                ],
                'choices'    => [4, 5, 6]
            ])
            ->add('ward_id', 'customSelect', [
                'label' => 'Phuong/Xa',
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => 'Chon Phuong/Xa',
                    'class' => 'form-control select-full',
                ],
                'choices'    => [7, 8, 9]
            ])

            ->add('status', 'customSelect', [
                'label'      => trans('core/base::tables.status'),
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'class' => 'form-control select-full',
                ],
                'choices'    => BaseStatusEnum::labels(),
            ])
            ->setBreakFieldPoint('status');
    }
}
