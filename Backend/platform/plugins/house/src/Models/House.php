<?php

namespace Botble\House\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;
use Botble\Base\Traits\UsesUuid;
use Botble\Rooms\Models\Rooms;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use EnumCastable;
    use UsesUuid;


    public $incrementing = false;
    // protected $hidden = ['id'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'houses';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'address',
        'province_id',
        'district_id',
        'ward_id',
        'status'

    ];

    /**
     * Get the rooms for the house .
     */
    public function rooms()
    {
        return $this->hasMany(Rooms::class, 'house_id');
    }

    /**
     * @var string
     */
    protected $screen = HOUSE_MODULE_SCREEN_NAME;

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];
}
