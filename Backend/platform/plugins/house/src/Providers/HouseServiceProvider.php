<?php

namespace Botble\House\Providers;

use Botble\House\Models\House;
use Illuminate\Support\ServiceProvider;
use Botble\House\Repositories\Caches\HouseCacheDecorator;
use Botble\House\Repositories\Eloquent\HouseRepository;
use Botble\House\Repositories\Interfaces\HouseInterface;
use Botble\Base\Supports\Helper;
use Event;
use Botble\Base\Traits\LoadAndPublishDataTrait;
use Illuminate\Routing\Events\RouteMatched;

class HouseServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function register()
    {
        $this->app->bind(HouseInterface::class, function () {
            return new HouseCacheDecorator(new HouseRepository(new House));
        });

        Helper::autoload(__DIR__ . '/../../helpers');
    }

    public function boot()
    {
        $this->setNamespace('plugins/house')
            ->loadAndPublishConfigurations(['permissions'])
            ->loadMigrations()
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadRoutes(['web']);

        Event::listen(RouteMatched::class, function () {
            dashboard_menu()->registerItem([
                'id'          => 'cms-plugins-house',
                'priority'    => 5,
                'parent_id'   => null,
                'name'        => 'plugins/house::house.name',
                'icon'        => 'fa fa-list',
                'url'         => route('house.index'),
                'permissions' => ['house.index'],
            ]);
        });
    }
}
