<?php

namespace Botble\House\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\House\Repositories\Interfaces\HouseInterface;

class HouseCacheDecorator extends CacheAbstractDecorator implements HouseInterface
{
    public function getAllHouse()
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

    public function getRoomsByHouseId(string $uuid)
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}
