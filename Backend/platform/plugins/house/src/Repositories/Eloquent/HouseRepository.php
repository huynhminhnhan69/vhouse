<?php

namespace Botble\House\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\House\Repositories\Interfaces\HouseInterface;
use Faker\Provider\Uuid;

class HouseRepository extends RepositoriesAbstract implements HouseInterface
{
    /**
     * @var string
     */
    protected $screen = HOUSE_MODULE_SCREEN_NAME;

    public function getAllHouse()
    {
        return $this->all();
    }

    /**
     * getRoomsByHouseId
     *
     * @return collection rooms
     */
    public function getRoomsByHouseId(string $uuid)
    {
        $house = $this->findOrFail($uuid);
        if ($house) {
            return $house->rooms()->get();
        }
        return collect();
    }
}
