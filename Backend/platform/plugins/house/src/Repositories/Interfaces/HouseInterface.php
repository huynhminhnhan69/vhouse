<?php

namespace Botble\House\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;
use Botble\House\Models\House;

interface HouseInterface extends RepositoryInterface
{
    /**
     * getAllHouse
     *
     * @return House collection  
     */
    public function getAllHouse();

    public function getRoomsByHouseId(string $uuid);
}
