<?php

Route::group(['namespace' => 'Botble\House\Http\Controllers', 'middleware' => 'web'], function () {

    Route::group(['prefix' => config('core.base.general.admin_dir'), 'middleware' => 'auth'], function () {

        Route::resource('houses', 'HouseController', ['names' => 'house']);

        Route::group(['prefix' => 'houses'], function () {

            Route::delete('items/destroy', [
                'as'         => 'house.deletes',
                'uses'       => 'HouseController@deletes',
                'permission' => 'house.destroy',
            ]);
        });
    });

});
