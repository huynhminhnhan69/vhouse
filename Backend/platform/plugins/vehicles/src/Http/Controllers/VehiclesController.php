<?php

namespace Botble\Vehicles\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Vehicles\Http\Requests\VehiclesRequest;
use Botble\Vehicles\Repositories\Interfaces\VehiclesInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\Vehicles\Tables\VehiclesTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Vehicles\Forms\VehiclesForm;
use Botble\Base\Forms\FormBuilder;

class VehiclesController extends BaseController
{
    /**
     * @var VehiclesInterface
     */
    protected $vehiclesRepository;

    /**
     * VehiclesController constructor.
     * @param VehiclesInterface $vehiclesRepository
     */
    public function __construct(VehiclesInterface $vehiclesRepository)
    {
        $this->vehiclesRepository = $vehiclesRepository;
    }

    /**
     * Display all vehicles
     * @param VehiclesTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(VehiclesTable $table)
    {

        page_title()->setTitle(trans('plugins/vehicles::vehicles.name'));

        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/vehicles::vehicles.create'));

        return $formBuilder->create(VehiclesForm::class)->renderForm();
    }

    /**
     * Insert new Vehicles into database
     *
     * @param VehiclesRequest $request
     * @return BaseHttpResponse
     */
    public function store(VehiclesRequest $request, BaseHttpResponse $response)
    {
        $vehicles = $this->vehiclesRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(VEHICLES_MODULE_SCREEN_NAME, $request, $vehicles));

        return $response
            ->setPreviousUrl(route('vehicles.index'))
            ->setNextUrl(route('vehicles.edit', $vehicles->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * Show edit form
     *
     * @param $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $vehicles = $this->vehiclesRepository->findOrFail($id);

        event(new BeforeEditContentEvent(VEHICLES_MODULE_SCREEN_NAME, $request, $vehicles));

        page_title()->setTitle(trans('plugins/vehicles::vehicles.edit') . ' "' . $vehicles->name . '"');

        return $formBuilder->create(VehiclesForm::class, ['model' => $vehicles])->renderForm();
    }

    /**
     * @param $id
     * @param VehiclesRequest $request
     * @return BaseHttpResponse
     */
    public function update($id, VehiclesRequest $request, BaseHttpResponse $response)
    {
        $vehicles = $this->vehiclesRepository->findOrFail($id);

        $vehicles->fill($request->input());

        $this->vehiclesRepository->createOrUpdate($vehicles);

        event(new UpdatedContentEvent(VEHICLES_MODULE_SCREEN_NAME, $request, $vehicles));

        return $response
            ->setPreviousUrl(route('vehicles.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $vehicles = $this->vehiclesRepository->findOrFail($id);

            $this->vehiclesRepository->delete($vehicles);

            event(new DeletedContentEvent(VEHICLES_MODULE_SCREEN_NAME, $request, $vehicles));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $vehicles = $this->vehiclesRepository->findOrFail($id);
            $this->vehiclesRepository->delete($vehicles);
            event(new DeletedContentEvent(VEHICLES_MODULE_SCREEN_NAME, $request, $vehicles));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }
}
