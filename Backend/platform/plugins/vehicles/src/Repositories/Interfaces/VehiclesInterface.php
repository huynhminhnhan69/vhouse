<?php

namespace Botble\Vehicles\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;

interface VehiclesInterface extends RepositoryInterface
{
}
