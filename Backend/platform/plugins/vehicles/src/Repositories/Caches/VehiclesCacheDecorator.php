<?php

namespace Botble\Vehicles\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Vehicles\Repositories\Interfaces\VehiclesInterface;

class VehiclesCacheDecorator extends CacheAbstractDecorator implements VehiclesInterface
{

}
