<?php

namespace Botble\Vehicles\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Vehicles\Repositories\Interfaces\VehiclesInterface;

class VehiclesRepository extends RepositoriesAbstract implements VehiclesInterface
{
    /**
     * @var string
     */
    protected $screen = VEHICLES_MODULE_SCREEN_NAME;
}
