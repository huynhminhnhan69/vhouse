<?php

namespace Botble\Vehicles\Providers;

use Botble\Vehicles\Models\Vehicles;
use Illuminate\Support\ServiceProvider;
use Botble\Vehicles\Repositories\Caches\VehiclesCacheDecorator;
use Botble\Vehicles\Repositories\Eloquent\VehiclesRepository;
use Botble\Vehicles\Repositories\Interfaces\VehiclesInterface;
use Botble\Base\Supports\Helper;
use Event;
use Botble\Base\Traits\LoadAndPublishDataTrait;
use Illuminate\Routing\Events\RouteMatched;

class VehiclesServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function register()
    {
        $this->app->bind(VehiclesInterface::class, function () {
            return new VehiclesCacheDecorator(new VehiclesRepository(new Vehicles));
        });

        Helper::autoload(__DIR__ . '/../../helpers');
    }

    public function boot()
    {
        $this->setNamespace('plugins/vehicles')
            ->loadAndPublishConfigurations(['permissions'])
            ->loadMigrations()
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadRoutes(['web']);

        Event::listen(RouteMatched::class, function () {
            dashboard_menu()->registerItem([
                'id'          => 'cms-plugins-vehicles',
                'priority'    => 5,
                'parent_id'   => null,
                'name'        => 'plugins/vehicles::vehicles.name',
                'icon'        => 'fa fa-list',
                'url'         => route('vehicles.index'),
                'permissions' => ['vehicles.index'],
            ]);
        });
    }
}
