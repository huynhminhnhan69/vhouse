<?php

return [
    [
        'name' => 'Vehicles',
        'flag' => 'vehicles.index',
    ],
    [
        'name' => 'Create',
        'flag' => 'vehicles.create',
        'parent_flag' => 'vehicles.index',
    ],
    [
        'name' => 'Edit',
        'flag' => 'vehicles.edit',
        'parent_flag' => 'vehicles.index',
    ],
    [
        'name' => 'Delete',
        'flag' => 'vehicles.destroy',
        'parent_flag' => 'vehicles.index',
    ],
];