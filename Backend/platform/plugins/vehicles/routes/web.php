<?php

Route::group(['namespace' => 'Botble\Vehicles\Http\Controllers', 'middleware' => 'web'], function () {

    Route::group(['prefix' => config('core.base.general.admin_dir'), 'middleware' => 'auth'], function () {

        Route::resource('vehicles', 'VehiclesController', ['names' => 'vehicles']);

        Route::group(['prefix' => 'vehicles'], function () {

            Route::delete('items/destroy', [
                'as'         => 'vehicles.deletes',
                'uses'       => 'VehiclesController@deletes',
                'permission' => 'vehicles.destroy',
            ]);
        });
    });

});
