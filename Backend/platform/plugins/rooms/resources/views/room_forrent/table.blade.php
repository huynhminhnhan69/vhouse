@extends('core/base::layouts.master')
@section('content')
    <div class="table-wrapper">
        @if ($table->isHasFilter())
            <div class="table-configuration-wrap" @if (request()->has('filter_table_id')) style="display: block;" @endif>
                <span class="configuration-close-btn btn-show-table-options"><i class="fa fa-times"></i></span>
                {!! $table->renderFilter() !!}
            </div>
        @endif
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-title">
                <div class="caption">
                    <div class="wrapper-action">
                        @if ($actions)
                            <div class="btn-group">
                                <a class="btn btn-secondary dropdown-toggle" href="#" data-toggle="dropdown">{{ trans('core/table::general.bulk_actions') }}
                                </a>
                                <ul class="dropdown-menu">
                                    @foreach ($actions as $action)
                                        <li>
                                            {!! $action !!}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($table->isHasFilter())
                            <button class="btn btn-primary btn-show-table-options">{{ trans('core/table::general.filters') }}</button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive @if ($actions) table-has-actions @endif @if ($table->isHasFilter()) table-has-filter @endif">
                    @section('main-table')
                        {!! $dataTable->table(compact('id', 'class'), false) !!}
                    @show
                </div>
            </div>
        </div>
    </div>
    @include('core/table::modal')
    {{-- // modal step  --}}

    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Thêm phòng cho thuê</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-md-offset-3">
                        <form id="msform">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active">Chọn nhà/phòng</li>
                                <li>Thêm khách thuê phòng</li>
                                <li>Account Setup</li>
                            </ul>
                            <!-- fieldsets -->
                            <fieldset class="form-body row text-left">
                                <div class="col-12">
                                <h2 class="fs-title text-weight-bold">Cài đặt phòng cho thuê</h2>
                                </div>
                                <div class="house_box form-group col-6">
                                    <label for="house_id" class="control-label required">Chọn nhà</label>
                                    <select class="form-control select-full ui-select ui-select select2-hidden-accessible mb-2"
                                     id="house_id" name="house_id" 
                                     tabindex="-1" aria-hidden="true"
                                     data-url="{{route('rooms.get.rooms.by.houseId')}}"
                                     >
                                        {{-- {{ $table->getListHouse()}} --}}
                                        <option value="">Chọn nhà</option>
                                        @foreach ($table->getListHouse() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="room_box form-group col-6">
                                    <label for="house_id" class="control-label required">Chọn phòng</label>
                                    <select class="form-control select-full ui-select ui-select select2-hidden-accessible mb-2" id="room_id" name="house_id" tabindex="-1" aria-hidden="true">
                                        <option value="">Chọn phòng</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label for="name" class="control-label required" aria-required="true">Tên</label>
                                    <input class="form-control" placeholder="Phòng 2 - Nhân" data-counter="120" name="name" type="text" value="" id="name" aria-invalid="false" aria-describedby="name-error">
                                </div>
                                <div class="form-group col-6">
                                    <label for="price" class="control-label required" aria-required="true">Giá phòng</label>
                                    <input class="form-control" placeholder="500.000vnđ" data-dismiss=="120" name="price" type="text" value="" id="price" aria-invalid="false" aria-describedby="name-error">
                                </div>
                                <div class="form-group col-6">
                                    <label for="down_payment" class="control-label" aria-required="true">Tiền cọc</label>
                                    <input class="form-control" placeholder="500.000vnđ" name="down_payment" type="text" value="" id="down_payment" aria-invalid="false" aria-describedby="name-error">
                                </div>
                                <div class="form-group col-6">
                                    <label for="start_date" class="control-label required" aria-required="true">Ngày dọn vào</label>
                                    <input class="form-control datepicker" placeholder="Ngày dọn vào" name="start_date" type="text" value="" data-date-format="dd/mm/yyyy" id="start_date" aria-invalid="false" aria-describedby="name-error">
                                </div>
                                <div class="form-group col-6">
                                    <label for="start_date" class="control-label" aria-required="true">Ngày dọn ra</label>
                                    <input class="form-control datepicker" placeholder="Ngày dọn vào" name="end_date" type="text" value="" data-date-format="dd/mm/yyyy" id="end_date" aria-invalid="false" aria-describedby="name-error">
                                </div>
                                <div class="form-group col-12">
                                    <label for="note" class="control-label" aria-required="true">Ghi chú</label>
                                    <textarea class="form-control" placeholder="Ghi chú" name="note" type="text" value=""  id="note" aria-invalid="false" aria-describedby="name-error"> </textarea>
                                </div>
                                
                                
                                <input type="button" name="next" class="next action-button" value="Next"/> 
                            </fieldset>

                            <fieldset class="form-body row text-left">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="fs-title text-weight-bold">Thêm khách thuê</h2>
                                        </div>
                                        <div class="col-12 text-right">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tanantModel">
                                                Thêm mới khách thuê
                                              </button>
                                        </div>
                                        
                                      
                                          
                                </div>
                                <div class="col-12">
                                    <div class="table-scrollable table-scrollable-agreement">
                                        <table class="table table-striped table-bordered table-hover items-table aside-table ">
                                          <thead>
                                          <tr>
                                            <th class="text-center">
                                              STT
                                            </th>
                                            <th>Tên khách thuê</th>
                                            <th class="text-center">SĐT (VN)</th>
                                            <th width="50">
                                            </th>
                                          </tr>
                                          </thead>
                                          <tbody class="renter-left-container">
                                          </tbody>
                                        </table>
                                      </div>
                                </div>
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                <input type="button" name="next" class="next action-button" value="Next"/>
                            </fieldset>
                            {{-- <fieldset>
                                <h2 class="fs-title">Thêm khách thuê phòng</h2>
                                <h3 class="fs-subtitle">Your presence on the social network</h3>
                                <input type="text" name="twitter" placeholder="Twitter"/>
                                <input type="text" name="facebook" placeholder="Facebook"/>
                                <input type="text" name="gplus" placeholder="Google Plus"/>
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                <input type="button" name="next" class="next action-button" value="Next"/>
                            </fieldset> --}}
                            <fieldset>
                                <h2 class="fs-title">Create your account</h2>
                                <h3 class="fs-subtitle">Fill in your credentials</h3>
                                <input type="text" name="email" placeholder="Email"/>
                                <input type="password" name="pass" placeholder="Password"/>
                                <input type="password" name="cpass" placeholder="Confirm Password"/>
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                <input type="submit" name="submit" class="submit action-button" value="Submit"/>
                            </fieldset>
                        </form>
                        <!-- link to designify.me code snippets -->
                      
                        <!-- /.link to designify.me code snippets -->
                    </div>
                </div>
                <!-- /.MultiStep Form -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      @include('plugins/tenant::tanantForm')
@stop
@section('javascript')
    {!! $dataTable->scripts() !!}
@stop
