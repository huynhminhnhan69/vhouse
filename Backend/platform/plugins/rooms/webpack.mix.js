let mix = require('laravel-mix');

const publicPath = 'public/vendor/core/plugins/rooms';
const resourcePath = './platform/plugins/rooms';

mix
    .js(resourcePath + '/resources/assets/js/rooms-forrent-table.js', publicPath + '/js')
    .copy(publicPath + '/js/rooms-forrent-table.js', resourcePath + '/public/js')

    .copyDirectory(resourcePath + '/resources/assets/css', publicPath + '/css');