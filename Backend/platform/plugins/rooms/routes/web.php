<?php

Route::group(['namespace' => 'Botble\Rooms\Http\Controllers', 'middleware' => 'web'], function () {

    Route::group(['prefix' => config('core.base.general.admin_dir'), 'middleware' => 'auth'], function () {

        Route::resource('rooms', 'RoomsController', ['names' => 'rooms']);


        Route::group(['prefix' => 'rooms'], function () {
            Route::delete('items/destroy', [
                'as'         => 'rooms.deletes',
                'uses'       => 'RoomsController@deletes',
                'permission' => 'rooms.destroy',
            ]);
        });
        Route::resource('rooms-forrent', 'RoomsForrentController', ['names' => 'rooms-forrent']);
        Route::group(['prefix' => 'rooms-forrent'], function () {
            Route::delete('items/destroy', [
                'as'         => 'rooms-forrent.deletes',
                'uses'       => 'RoomsController@deletes',
                'permission' => 'rooms-forrent.destroy',
            ]);
        });
    });
    Route::get('get-rooms-by-house-id', [
        'as'         => 'rooms.get.rooms.by.houseId',
        'uses'       => 'RoomsController@GetRoomsByHouseId',
        'permission' => 'rooms-forrent.index',
    ]);
});
