<?php

return [
    [
        'name' => 'Rooms',
        'flag' => 'rooms.index',
    ],
    [
        'name' => 'Create',
        'flag' => 'rooms.create',
        'parent_flag' => 'rooms.index',
    ],
    [
        'name' => 'Edit',
        'flag' => 'rooms.edit',
        'parent_flag' => 'rooms.index',
    ],
    [
        'name' => 'Delete',
        'flag' => 'rooms.destroy',
        'parent_flag' => 'rooms.index',
    ],
];