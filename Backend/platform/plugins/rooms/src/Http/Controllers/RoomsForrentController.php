<?php

namespace Botble\Rooms\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Rooms\Http\Requests\RoomsRequest;
use Botble\Rooms\Repositories\Interfaces\RoomsInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\Rooms\Tables\RoomsTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Rooms\Forms\RoomsForm;
use Botble\Base\Forms\FormBuilder;
use Botble\Rooms\Tables\RoomsForrentTable;

class RoomsForrentController extends BaseController
{
    /**
     * @var RoomsInterface
     */
    protected $roomsRepository;

    /**
     * RoomsController constructor.
     * @param RoomsInterface $roomsRepository
     */
    public function __construct(RoomsInterface $roomsRepository)
    {
        $this->roomsRepository = $roomsRepository;
    }

    /**
     * Display all rooms
     * @param RoomsTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(RoomsForrentTable $table)
    {
        page_title()->setTitle('Quản lý thuê phòng');
        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/rooms::rooms.create'));

        return $formBuilder->create(RoomsForm::class)->renderForm();
    }

    /**
     * Insert new Rooms into database
     *
     * @param RoomsRequest $request
     * @return BaseHttpResponse
     */
    public function store(RoomsRequest $request, BaseHttpResponse $response)
    {
        $rooms = $this->roomsRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(ROOMS_MODULE_SCREEN_NAME, $request, $rooms));

        return $response
            ->setPreviousUrl(route('rooms.index'))
            ->setNextUrl(route('rooms.edit', $rooms->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * Show edit form
     *
     * @param $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $rooms = $this->roomsRepository->findOrFail($id);

        event(new BeforeEditContentEvent(ROOMS_MODULE_SCREEN_NAME, $request, $rooms));

        page_title()->setTitle(trans('plugins/rooms::rooms.edit') . ' "' . $rooms->name . '"');

        return $formBuilder->create(RoomsForm::class, ['model' => $rooms])->renderForm();
    }

    /**
     * @param $id
     * @param RoomsRequest $request
     * @return BaseHttpResponse
     */
    public function update($id, RoomsRequest $request, BaseHttpResponse $response)
    {
        $rooms = $this->roomsRepository->findOrFail($id);

        $rooms->fill($request->input());

        $this->roomsRepository->createOrUpdate($rooms);

        event(new UpdatedContentEvent(ROOMS_MODULE_SCREEN_NAME, $request, $rooms));

        return $response
            ->setPreviousUrl(route('rooms.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $rooms = $this->roomsRepository->findOrFail($id);

            $this->roomsRepository->delete($rooms);

            event(new DeletedContentEvent(ROOMS_MODULE_SCREEN_NAME, $request, $rooms));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $rooms = $this->roomsRepository->findOrFail($id);
            $this->roomsRepository->delete($rooms);
            event(new DeletedContentEvent(ROOMS_MODULE_SCREEN_NAME, $request, $rooms));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }
}
