<?php

namespace Botble\Rooms\Tables;

use Auth;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Rooms\Repositories\Interfaces\RoomsForrentInterface;
use Botble\Table\Abstracts\TableAbstract;
use Illuminate\Contracts\Routing\UrlGenerator;
use Yajra\DataTables\DataTables;
use Botble\House\Repositories\Interfaces\HouseInterface;
use Botble\Assets\Assets;

class RoomsForrentTable extends TableAbstract
{

    /**
     * @var bool
     */
    protected $hasActions = true;

    /**
     * @var bool
     */
    protected $hasFilter = true;

    /**
     * @var string
     */
    protected $view = 'plugins/rooms::room_forrent.table';

    /**
     * houseRepository
     *
     * @var HouseInterface
     */
    public $houseRepository;

    /**
     * RoomsTable constructor.
     * @param DataTables $table
     * @param UrlGenerator $urlDevTool
     * @param RoomsForrentInterface $roomsForrentRepository
     */
    public function __construct(DataTables $table, UrlGenerator $urlDevTool, RoomsForrentInterface $roomsForrentRepository, HouseInterface $houseRepository)
    {
        $this->repository = $roomsForrentRepository;
        $this->houseRepository = $houseRepository;
        $this->setOption('id', 'table-plugins-rooms-forrent');
        parent::__construct($table, $urlDevTool);

        if (!Auth::user()->hasAnyPermission(['rooms-forrent.edit', 'rooms-forrent.destroy'])) {
            $this->hasOperations = false;
            $this->hasActions = false;
        }
    }

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     * @since 2.1
     */
    public function ajax()
    {
        $data = $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                if (!Auth::user()->hasPermission('rooms-forrent.edit')) {
                    return $item->name;
                }
                return anchor_link(route('rooms-forrent.edit', $item->id), $item->name);
            })
            ->editColumn('checkbox', function ($item) {
                return table_checkbox($item->id);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at, config('core.base.general.date_format.date'));
            })
            ->editColumn('status', function ($item) {
                return $item->status->toHtml();
            });

        return apply_filters(BASE_FILTER_GET_LIST_DATA, $data, ROOMS_MODULE_SCREEN_NAME)
            ->addColumn('operations', function ($item) {
                return table_actions('rooms-forrent.edit', 'rooms-forrent.destroy', $item);
            })
            ->escapeColumns([])
            ->make(true);
    }


    /**
     * getListHouse
     *
     * @return array house
     */
    public function getListHouse()
    {
        $listHouse = $this->houseRepository->all();
        return $listHouse;
    }
    /**
     * Get the query object to be processed by table.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     * @since 2.1
     */
    public function query()
    {
        $model = $this->repository->getModel();
        $query = $model->select([
            'id',
            'name',
            'created_at',
            'status',
        ]);

        return $this->applyScopes(apply_filters(BASE_FILTER_TABLE_QUERY, $query, $model, ROOMS_MODULE_SCREEN_NAME));
    }

    /**
     * @return array
     * @since 2.1
     */
    public function columns()
    {
        return [
            'id' => [
                'name' => 'rooms-forrent.id',
                'title' => trans('core/base::tables.id'),
                'width' => '20px',
            ],
            'name' => [
                'name' => 'rooms-forrent.name',
                'title' => trans('core/base::tables.name'),
                'class' => 'text-left',
            ],
            'created_at' => [
                'name' => 'rooms-forrent.created_at',
                'title' => trans('core/base::tables.created_at'),
                'width' => '100px',
            ],
            'status' => [
                'name' => 'rooms-forrent.status',
                'title' => trans('core/base::tables.status'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * @return array
     * @since 2.1
     * @throws \Throwable
     */
    public function buttons()
    {
        $buttons = $this->addCreateButton(route('rooms-forrent.create'), 'rooms-forrent.create');
        //  $buttons = $this->addCreateButton(route('rooms-forrent.create'), 'rooms-forrent.create');

        return apply_filters(BASE_FILTER_TABLE_BUTTONS, $buttons, ROOMS_MODULE_SCREEN_NAME);
    }

    /**
     * @param array $buttons
     * @param string $url
     * @param null $permission
     * @throws \Throwable
     */
    protected function addCreateButton(string $url, $permission = null, array $buttons = []): array
    {
        if (!$permission || Auth::user()->hasPermission($permission)) {
            $buttons['add-room-forrent'] = ['text' => '<i class="fa fa-plus"></i> <a href="#" class="add-room-forrent">Tạo mới</a>'];
        }

        return $buttons;
    }

    public function render($view, $data = [], $mergeData = [])
    {
        \Assets::addScriptsDirectly('/vendor/core/plugins/rooms/js/rooms-forrent-table.js');
        \Assets::addStylesDirectly('/vendor/core/plugins/rooms/css/rooms-forrent-table.css');
        \Assets::addScriptsDirectly('/vendor/core/plugins/tenant/js/tenant.js');

        return parent::render($view, $data, $mergeData);
    }
    /**
     * @return array
     * @throws \Throwable
     */
    public function bulkActions(): array
    {
        return $this->addDeleteAction(route('rooms-forrent.deletes'), 'rooms-forrent.destroy', parent::bulkActions());
    }

    /**
     * @return array
     */
    public function getBulkChanges(): array
    {
        return [
            'rooms-forrent.name' => [
                'title'    => trans('core/base::tables.name'),
                'type'     => 'text',
                'validate' => 'required|max:120',
                'callback' => 'getNames',
            ],
            'rooms-forrent.status' => [
                'title'    => trans('core/base::tables.status'),
                'type'     => 'select',
                'choices'  => BaseStatusEnum::labels(),
                'validate' => 'required|in:' . implode(',', BaseStatusEnum::values()),
            ],
            'rooms-forrent.created_at' => [
                'title' => trans('core/base::tables.created_at'),
                'type'  => 'date',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getNames()
    {
        return $this->repository->pluck('rooms-forrent.name', 'rooms-forrent.id');
    }
}
