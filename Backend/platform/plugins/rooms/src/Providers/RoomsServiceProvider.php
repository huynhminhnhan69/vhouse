<?php

namespace Botble\Rooms\Providers;

use Botble\Rooms\Models\Rooms;
use Illuminate\Support\ServiceProvider;
use Botble\Rooms\Repositories\Caches\RoomsCacheDecorator;
use Botble\Rooms\Repositories\Eloquent\RoomsRepository;
use Botble\Rooms\Repositories\Interfaces\RoomsInterface;
use Botble\Rooms\Repositories\Caches\RoomsForrentCacheDecorator;
use Botble\Rooms\Repositories\Eloquent\RoomsForrentRepository;
use Botble\Rooms\Repositories\Interfaces\RoomsForrentInterface;
use Botble\Rooms\Models\RoomsForrent;
use Botble\Base\Supports\Helper;
use Event;
use Botble\Base\Traits\LoadAndPublishDataTrait;
use Illuminate\Routing\Events\RouteMatched;

class RoomsServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function register()
    {
        $this->app->bind(RoomsInterface::class, function () {
            return new RoomsCacheDecorator(new RoomsRepository(new Rooms));
        });

        $this->app->bind(RoomsForrentInterface::class, function () {
            return new RoomsForrentCacheDecorator(new RoomsForrentRepository(new RoomsForrent));
        });

        Helper::autoload(__DIR__ . '/../../helpers');
    }

    public function boot()
    {
        $this->setNamespace('plugins/rooms')
            ->loadAndPublishConfigurations(['permissions'])
            ->loadMigrations()
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadRoutes(['web']);

        Event::listen(RouteMatched::class, function () {
            dashboard_menu()->registerItem(
                [
                    'id'          => 'cms-plugins-rooms',
                    'priority'    => 5,
                    'parent_id'   => null,
                    'name'        => 'Quản lý phòng trọ',
                    'icon'        => 'fa fa-list',
                    'url'         => null,
                    'permissions' => ['rooms.index'],
                ]
            );
            dashboard_menu()->registerItem(
                [
                    'id'          => 'cms-plugins-rooms-child',
                    'priority'    => 1,
                    'parent_id'   => 'cms-plugins-rooms',
                    'name'        => 'Quản lý phòng',
                    'icon'        => 'fa fa-list',
                    'url'         => route('rooms.index'),
                    'permissions' => ['rooms.index'],
                ]
            );
            dashboard_menu()->registerItem(
                [
                    'id'          => 'cms-plugins-rooms-forrent',
                    'priority'    => 2,
                    'parent_id'   => 'cms-plugins-rooms',
                    'name'        => 'Quản lý thuê phòng',
                    'icon'        => 'fa fa-list',
                    'url'         => route('rooms-forrent.index'),
                    'permissions' => ['rooms.index'],
                ]
            );
        });
    }
}
