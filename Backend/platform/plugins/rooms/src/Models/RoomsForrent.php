<?php

namespace Botble\Rooms\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;
use Botble\Base\Traits\UsesUuid;

class RoomsForrent extends BaseModel
{
    use EnumCastable;
    use UsesUuid;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rooms_forrent';

    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'room_id',
        'price',
        'down_payment',
        'room_date_empty',
        'start_date',
        'end_date',
        'note',
    ];



    /**
     * @var string
     */
    protected $screen = ROOMS_FORRENT_MODULE_SCREEN_NAME;

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];
}
