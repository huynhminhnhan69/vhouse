<?php

namespace Botble\Rooms\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;
use Botble\Base\Traits\UsesUuid;

class Rooms extends BaseModel
{
    use EnumCastable;
    use UsesUuid;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rooms';

    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'house_id',
        'max_people',
        'acreage',
        'floor_number',
        'status',
    ];

    /**
     * @var string
     */
    protected $screen = ROOMS_MODULE_SCREEN_NAME;

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];
}
