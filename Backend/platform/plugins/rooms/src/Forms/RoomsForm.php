<?php

namespace Botble\Rooms\Forms;

use Botble\Base\Forms\FormAbstract;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Rooms\Http\Requests\RoomsRequest;
use Botble\House\Repositories\Interfaces\HouseInterface;

class RoomsForm extends FormAbstract
{

    /**
     * @return mixed|void
     * @throws \Throwable
     */
    public function buildForm()
    {
        $houseResponsitory = app(HouseInterface::class);
        $houseCollection = $houseResponsitory->all()->pluck('name', 'id')->toArray();
        $this
            ->setModuleName(ROOMS_MODULE_SCREEN_NAME)
            ->setValidatorClass(RoomsRequest::class)
            ->withCustomFields()
            ->add('name', 'text', [
                'label' => trans('core/base::forms.name'),
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => trans('core/base::forms.name_placeholder'),
                    'data-counter' => 120,
                ],
            ])
            ->add('house_id', 'customSelect', [
                'label' =>  'Nha',
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'class' => 'form-control select-full',
                ],
                'choices'    => $houseCollection
            ])
            ->add('max_people', 'number', [
                'label' => 'So luong nguoi toi da/phong',
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => 'So nguoi toi da/phong',
                    'data-counter' => 2,
                ],
            ])
            ->add('acreage', 'number', [
                'label' => 'Dien tich',
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'placeholder'  => 'Dien tich (m2)',
                    'data-counter' => 2,
                ],
            ])
            ->add('floor_number', 'number', [
                'label' => 'Lau so',
                'label_attr' => ['class' => 'control-label'],
                'attr' => [
                    'placeholder'  => 'Tang',
                    'data-counter' => 2,
                ],
            ])
            ->add('status', 'customSelect', [
                'label'      => trans('core/base::tables.status'),
                'label_attr' => ['class' => 'control-label required'],
                'attr' => [
                    'class' => 'form-control select-full',
                ],
                'choices'    => BaseStatusEnum::labels(),
            ])
            ->setBreakFieldPoint('status');
    }
}
