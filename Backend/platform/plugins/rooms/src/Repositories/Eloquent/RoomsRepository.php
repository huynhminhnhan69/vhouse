<?php

namespace Botble\Rooms\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Rooms\Repositories\Interfaces\RoomsInterface;

class RoomsRepository extends RepositoriesAbstract implements RoomsInterface
{
    /**
     * @var string
     */
    protected $screen = ROOMS_MODULE_SCREEN_NAME;
}
