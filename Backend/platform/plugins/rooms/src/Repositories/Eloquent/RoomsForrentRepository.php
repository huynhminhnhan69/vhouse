<?php

namespace Botble\Rooms\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Rooms\Repositories\Interfaces\RoomsForrentInterface;

class RoomsForrentRepository extends RepositoriesAbstract implements RoomsForrentInterface
{
    /**
     * @var string
     */
    protected $screen = ROOMS_FORRENT_MODULE_SCREEN_NAME;
}
