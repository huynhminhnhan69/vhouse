<?php

namespace Botble\Rooms\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Rooms\Repositories\Interfaces\RoomsForrentInterface;

class RoomsForrentCacheDecorator extends CacheAbstractDecorator implements RoomsForrentInterface
{
}
