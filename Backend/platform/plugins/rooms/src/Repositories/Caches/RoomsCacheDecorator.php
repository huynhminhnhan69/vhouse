<?php

namespace Botble\Rooms\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Rooms\Repositories\Interfaces\RoomsInterface;

class RoomsCacheDecorator extends CacheAbstractDecorator implements RoomsInterface
{
}
