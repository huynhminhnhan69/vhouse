<?php

if (!defined('ROOMS_MODULE_SCREEN_NAME')) {
    define('ROOMS_MODULE_SCREEN_NAME', 'rooms');
}
if (!defined('ROOMS_FORRENT_MODULE_SCREEN_NAME')) {
    define('ROOMS_FORRENT_MODULE_SCREEN_NAME', 'rooms-forrent');
}
