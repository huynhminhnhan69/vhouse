<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateUuidTypeHouseIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('rooms', 'house_id')) {

            Schema::table('rooms', function (Blueprint $table) {
                $table->dropColumn('house_id');
            });
            Schema::table('rooms', function (Blueprint $table) {
                $table->uuid('house_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
