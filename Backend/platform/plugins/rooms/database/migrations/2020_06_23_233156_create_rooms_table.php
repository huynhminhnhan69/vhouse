<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 120);
            $table->float('price');
            $table->integer('house_id');
            $table->integer('acreage'); // dien tich
            $table->integer('max_people');
            $table->float('deposit');  //tien coc 
            $table->date('room_date_empty');
            $table->integer('floor_id');
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('floors', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 120);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
        Schema::dropIfExists('floor');
    }
}
