<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateFloorIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasColumn('rooms', 'floor_id')) {

            Schema::table('rooms', function (Blueprint $table) {
                $table->dropColumn('floor_id');
            });

            Schema::table('rooms', function (Blueprint $table) {
                $table->integer('floor_number')->nullable();
            });
        }

        Schema::dropIfExists('floor');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
