<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableRoomsForrent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('rooms_forrent', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 120);
            $table->uuid('room_id');
            $table->decimal('price', 9, 3);
            $table->decimal('down_payment', 11, 3);  //tien coc 
            $table->date('room_date_empty');
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->char('note');
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('deposit');
            $table->dropColumn('room_date_empty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms_forrent');
    }
}
