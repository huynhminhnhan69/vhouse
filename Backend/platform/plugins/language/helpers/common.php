<?php

use Botble\Language\Repositories\Interfaces\LanguageMetaInterface;

if (!function_exists('remove_query_string_var')) {
    /**
     * @param $url
     * @param $key
     * @return bool|mixed|string
     */
    function remove_query_string_var($url, $key)
    {
        if (!is_array($key)) {
            $key = [$key];
        }
        foreach ($key as $item) {
            $url = preg_replace('/(.*)(?|&)' . $item . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&');
            $url = substr($url, 0, -1);
        }
        return $url;
    }
}

if (!function_exists('remaining_meta_language')) {
    /**
     * @param $screen               SCREEN should be retrieving data
     * @param $meta_code_source     locale source
     * @param $meta_code_source     locale destination
     * @param $meta_content_id      id of object to get data
     * @return bool|Langmeta
     */
    function remaining_meta_language(string $screen, string $meta_code_source, string $meta_code_destination, int $meta_content_id)
    {
        $languageMeta1st = resolve(LanguageMetaInterface::class)
                                    ->getFirstBy(['lang_meta_reference' => $screen, 
                                                  'lang_meta_content_id' => $meta_content_id,
                                                  'lang_meta_code' => $meta_code_source]);
        if(!$languageMeta1st){
            return false;
        }
        $languageMeta2nd = resolve(LanguageMetaInterface::class)
                                    ->getFirstBy(['lang_meta_reference' => $screen,
                                                  'lang_meta_origin' => $languageMeta1st->lang_meta_origin, 
                                                  'lang_meta_code' => $meta_code_destination]);
        if(!$languageMeta2nd){
            return false;
        }
        return $languageMeta2nd;
    }
}


