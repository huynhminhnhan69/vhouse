<?php
namespace Deployer;

require 'recipe/symfony.php';

require_once(__DIR__ .'/readEnv.php');
$env = new \readEnv();
$env->convertEnv('.env_deploy');

set('application', $env->getEnv('DEPLOY_APPLICATION'));
set('default_timeout', $env->getEnv('DEPLOY_TIMEOUT') ?? 2000);
set('git_tty', true);
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);

task('deploy:dev', [
    // 'deploy:start',
    'deploy:upload',
    'deploy:permission',
    // 'deploy:completed'
]);

if($env->getEnv('APP_ENV') == 'prod'){
    host($env->getEnv('PROD_DEPLOY_HOST'))
      ->user($env->getEnv('PROD_DEPLOY_USER'))
      ->port($env->getEnv('PROD_DEPLOY_PORT'))
      ->identityFile($env->getEnv('PROD_DEPLOY_CERTIFICATE'))
      ->set('deploy_path', $env->getEnv('PROD_DEPLOY_PATH'))
      ->set('permission', $env->getEnv('PROD_DEPLOY_PERMISSION'))
      ->set('app_env', $env->getEnv('APP_ENV'))
      ->multiplexing(true);
}else{
    host($env->getEnv('DEPLOY_HOST'))
      ->user($env->getEnv('DEPLOY_USER'))
      ->port($env->getEnv('DEPLOY_PORT'))
      ->identityFile($env->getEnv('DEPLOY_CERTIFICATE'))
      ->set('deploy_path', $env->getEnv('DEPLOY_PATH'))
      ->set('permission', $env->getEnv('DEPLOY_PERMISSION'))
      ->set('app_env', $env->getEnv('APP_ENV'))
      ->multiplexing(true);
}

task('deploy:start', function()
{
    if(get('app_env') == 'prod'){
        writeln('**************************************');
        writeln('*     Application In Production!     *');
        writeln('**************************************');
        
        if(!askConfirmation('Do you really wish to run this command?')) {
            writeln('Command Cancelled!');
            die;
        }
    }
    writeln('Start deployer');
    $path = get('deploy_path');
    run("cd \"{$path}\" && php artisan down --message=\"Upgrading Database\" --retry=60");
});

task('deploy:upload', function(){ // 
    writeln('Start upload');

    $folders = [
       'app',
       'config',
       'bootstrap',
       'database',
       'platform',
       'resources',
       'routes',
       'storage',
       'public',
       'public',
       'tests',
       'vendor'
    ];

    $files = [
      'server.php',
      '.env.example',
      '.htaccess',
      'artisan',
      'phpunit.xml',
      '.editorconfig',
      'composer.json',
      'composer.lock',
    ];

    $path = get('deploy_path');
    foreach ($folders as $key => $folder) {
        # code...
        upload("{$folder}//", $path."//{$folder}");
    }

    foreach ($files as $key => $file) {
        # code...
        upload("{$file}", $path."//{$file}");
    }

});

task('deploy:build', function(){ // 
    writeln('Start build');
    try {
        $path = get('deploy_path');
        run("cd \"{$path}\" && COMPOSER_PROCESS_TIMEOUT=2000 composer install");
    } catch (\Symfony\Component\Process\Exception\ProcessFailedException $e) {
        writeln('Restore...');
    }
});

task('deploy:permission', function(){
    $permission = get('permission');
    $path       = get('deploy_path');
    $folders = [
        'app',
        'config',
        'bootstrap',
        'database',
        'platform',
        'resources',
        'routes',
        'storage',
        'public',
        'tests'
    ];

    foreach ($folders as $key => $folder) {
        # code...
        run('chown -R '.$permission.' '.$path. "/{$folder}");
    }
});

task('deploy:restore', function(){
  
});

task('deploy:completed', function(){
  $path = get('deploy_path');
  run("cd \"{$path}\" && php artisan up");
});

after('deploy:failed', 'deploy:restore');