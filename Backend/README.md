<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# PROJECT 007 Flower

## Install source
**Install vendor packages**

* composer install

* npm install

**Update your configuration**

* Create .env file from .env-example

**Create database structure**

* php artisan migrate

**Active theme**

* php artisan cms:theme:activate flower

**Active PHPCS**

* php artisan git:install-hooks

**Create admin user**

* php artisan cms:user:create

**Create initial product origin**

* php artisan cms:product-origin:create

**Create initial administrative data**

* php artisan cms:administrative-unit:create

**Create initial category blog data**

* php artisan category:migrate-data

**Create initial menu**

* php artisan cms:menu:create


**Publish front-end**

* php artisan vendor:publish --tag=cms-public --force

* php artisan cms:theme:assets:publish flower

* php artisan storage:link

 Run the first test with command php artisan serve. Open http://localhost:8000, you should see home page of / and admin at /admin

## Build bundle

* Run npm install to download node_modules

Run it when you wanna build common libary themes
* npm run build-flower-package

Run it when you wanna build package
* npm run build-package

## Local build

* Run chmod +x deploy.sh
* Add special package you wanna build follow structure
* Open terminal and Run ./deploy.sh

## Deployer

* Create .env_deploy file from .env_deploy.example and update your configuration

In deploy.php
The first deploy, you have ignore "deploy:start" & "deploy:completed" because server doesn't install composer. You can't create step auto install composer also auto bundle on server because server is very "lô" =)). Please do it manual on your local and push folder vendor to server.
After that, you can run  "deploy:start" & "deploy:completed"
* Run dep deploy:dev

## Plugins activate

* php artisan cms:plugin:activate <name plugin>

## Experiences

Run composer prod
* ea-php72 /opt/cpanel/composer/bin/composer install


Customize php version

<FilesMatch \.php$>
    # Apache 2.4.10+ can proxy to unix socket
    SetHandler "proxy:unix:/var/run/php/php7.1-fpm.sock|fcgi://adev-flower.bigin.top/"
</FilesMatch>
sudo update-alternatives --set php /usr/bin/php7.1

