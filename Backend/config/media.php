<?php

return [
    'sizes' => [
        'thumb'    => '150x150',
        'banner' => '1440x960',
        'medium' => '840x500'
    ]
];