
<?php echo Theme::partial('header'); ?>


<main class="main" id="main">
    <div class="container">
        <?php if(Route::currentRouteName() == 'public.single' && is_plugin_active('blog')): ?>
            <?php
                $featured = get_featured_posts(5);
            ?>
            <?php if(count($featured) > 0): ?>
                <div class="main-feature">
                    <?php $__currentLoopData = $featured; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="feature-item">
                            <div class="feature-item-dv">
                                <a href="<?php echo e(route('public.single', $feature_item->slug)); ?>"
                                   title="<?php echo e($feature_item->name); ?>"
                                   style="display: block">
                                    <img class="img-full img-bg" src="<?php echo e(get_object_image($feature_item->image, $loop->first ? 'featured' : 'medium')); ?>" alt="<?php echo e($feature_item->name); ?>"
                                         style="background-image: url('<?php echo e(get_object_image($feature_item->image)); ?>');">
                                    <span class="feature-item-link"
                                          title="<?php echo e($feature_item->name); ?>">
                                        <span><?php echo e($feature_item->name); ?></span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <div class="main-content">
            <div class="main-left">
                <?php echo Theme::content(); ?>

            </div>
            <?php echo Theme::partial('sidebar'); ?>

        </div>
    </div>
</main>

<?php echo Theme::partial('footer'); ?>


<?php /**PATH /Users/huynhminhnhan/Documents/Chouse/BE/platform/themes/newstv/layouts/default.blade.php ENDPATH**/ ?>