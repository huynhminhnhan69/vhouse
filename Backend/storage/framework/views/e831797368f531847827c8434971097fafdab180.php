<?php if(is_plugin_active('blog')): ?>
    <?php $__currentLoopData = get_all_categories(['categories.status' => \Botble\Base\Enums\BaseStatusEnum::PUBLISHED, 'categories.parent_id' => 0, 'is_featured' => 1]); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $allRelatedCategoryIds = array_unique(array_merge(app(\Botble\Blog\Repositories\Interfaces\CategoryInterface::class)->getAllRelatedChildrenIds($category), [$category->id]));

            $posts = app(\Botble\Blog\Repositories\Interfaces\PostInterface::class)->getByCategory($allRelatedCategoryIds, 0, $loop->index % 2 == 0 ? 6 : 5);
        ?>
        <section class="main-box">
            <div class="main-box-header">
                <div class="btn-group">
                    <button type="button"
                            class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        Select <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <?php $__currentLoopData = $category->children()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <a href="<?php echo e(route('public.single', $child_category->slug)); ?>"
                               title="<?php echo e($child_category->name); ?>">
                                <?php echo e($child_category->name); ?>

                            </a>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
                <h2>
                    <i class="fa fa-leaf"></i> <?php echo e($category->name); ?>

                </h2>
            </div>
            <div class="main-box-content">
                <div class="box-style box-style-<?php echo e($loop->index % 2 == 0 ? 2 : 1); ?>">
                    <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="media-news">
                        <a href="<?php echo e(route('public.single', $post->slug)); ?>"
                           title="<?php echo e($post->name); ?>"
                           class="media-news-img">
                            <img class="img-full img-bg"
                                 src="<?php echo e(get_object_image($post->image, 'medium')); ?>"
                                 style="background-image: url('<?php echo e(get_object_image($post->image)); ?>');"
                                 alt="<?php echo e($post->name); ?>">
                        </a>
                        <div class="media-news-body">
                            <p class="common-title">
                                <a href="<?php echo e(route('public.single', $post->slug)); ?>"
                                   title="<?php echo e($post->name); ?>">
                                    <?php echo e($post->name); ?>

                                </a>
                            </p>
                            <p class="common-date">
                                <time><?php echo e(date_from_database($post->created_at, 'M d, Y')); ?></time>
                            </p>
                            <div class="common-summary">
                                <p><?php echo e($post->description); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </section>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?><?php /**PATH /Users/huynhminhnhan/Documents/Chouse/BE/platform/themes/newstv/views/index.blade.php ENDPATH**/ ?>