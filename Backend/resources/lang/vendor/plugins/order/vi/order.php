<?php

return [
    'name' => 'Đơn hàng',
    'create' => 'Tạo mới đơn hàng',
    'edit' => 'Chỉnh sửa đơn hàng',
    'status'    =>[
        'awaiting_confirmation' => 'Đang chờ xác nhận',
        'confirmed'         => 'Đã xác nhận',
        'on_delivery'       => 'Đang vận chuyển',
        'completed'         => 'Hoàn thành',
        'canceled'          => 'Đã hủy'
    ],
    'delivery'              => [
        'delivery'          => 'Giao hàng',
        'in_store_pickup'   => 'Nhận tại cửa hàng',
        'in_store_shopping' => 'Mua tại cửa hàng',
        'for_others'        => 'Tặng cho người khác',
    ],
    'payment'               => [
        'bank_transfer'     => 'Chuyển khoản',
        'payment_getway'    => 'Cổng thanh toán',
        'cash'              => 'Thanh toán bằng tiền mặt',
        'cod'               => 'Thanh toán bằng tiền mặt khi nhận hàng',
        'pos'               => 'Thanh toán bằng thẻ'
    ],
    'choose_status'         => 'Chọn trạng thái',
    'choose_delivery'       => 'Chọn hình thức giao hàng',
    'choose_payment'        => 'Chọn phương thức thanh toán',
    'settings'               => [
        'title'             => 'Hoá đơn',
        'description'       => 'Cài đặt thuế giá trị gia tăng vat(%)',
        'vat'               => 'Vat(%)',
    ],
    'date_purchase'         => 'Ngày mua',
    'order_status'          => 'Trạng thái đơn hàng',
    "order_date"            => 'Ngày đặt hàng',



    'customer_name.required'    => 'Họ tên khách hàng không được để trống',
    'customer_email.required'   => 'Email không được để trống',
    'customer_phone.required'   => 'Điện thoại không được để trống',
    'customer_company.max'      => 'Địa chỉ không được dài quá 255 ký tự',
    'customer_address.required' => 'Địa chỉ không được để trống',
    'customer_province.required'=> 'Tỉnh/Thành Phố không được để trống',
    'customer_district.required'=> 'Quận/Huyện không được để trống',
    'customer_ward.required'    => 'Xã/Phường không được để trống',
    'delivery_by.required'      => 'Vui lòng chọn hình thức nhận hàng',

    'receiver_name.required'    => 'Tên người nhận không được để trống',
    'receiver_email.required'   => 'Email người nhận không được để trống',
    'receiver_phone.required'   => 'Số điện thoại người nhận không được để trống',
    'receiver_company.max'      => 'Địa chỉ quá dài',
    'receiver_address.required' => 'Địa chỉ người nhận không được để trống',
    'receiver_province.required'=> 'Tỉnh/Thành Phố không được để trống',
    'receiver_district.required'=> 'Quận/Huyện không được để trống',
    'receiver_ward.required'    => 'Xã/Phường không được để trống',
    'delivery_date.required'    => 'Ngày giao hàng không được để trống',
    'delivery_time.required'    => 'Chọn thời gian giao hàng'




];
