<?php
return [
    'name'  => 'Hoá đơn',
    'code'  => 'Mã hoá đơn',
    'created_at'    => 'Ngày xuất hoá đơn',
    'settings'      => [
        'title'     => 'Cài đặt thông tin hoá đơn',
        'company_name'  => 'Tên công ty',
        'tax_code'      => 'Mã số thuế',
        'address'       => 'Địa chỉ',
        'phone'         => 'Điện thoại',
        'fax'           => 'Fax',
        'bank_account'  => 'Số tài khoản',
        'bank'          => 'Tại ngân hàng'
    ],
    'invoice_informations'  => 'Thông tin hoá đơn'
];
?>