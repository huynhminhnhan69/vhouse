<?php
    return [
        'name'  => 'Invoices',
        'code'  => 'Invoice Code',
        'created_at'   => 'Created At',
        'settings'      => [
            'title'     => 'Invoice Setting',
            'company_name'  => 'Company name',
            'tax_code'      => 'Tax code',
            'address'       => 'Address',
            'phone'         => 'Phone',
            'fax'           => 'Fax',
            'bank_account'  => 'Bank account',
            'bank'          => 'Bank'
        ],
        'invoice_informations'  => 'Invoice informations'
    ];
?>