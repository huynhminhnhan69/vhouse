<?php

return [
    'name' => 'Order',
    'create' => 'New order',
    'edit' => 'Edit order',
    'status'    =>[
        'awaiting_confirmation' => 'Awaiting Confirmation',
        'confirmed'         => 'Confirmed',
        'on_delivery'       => 'On delivery',
        'completed'         => 'Completed',
        'canceled'          => 'Canceled'
    ],
    'delivery'              => [
        'delivery'          => 'Delivery',
        'in_store_pickup'   => 'In-store pickup',
        'in_store_shopping' => 'In-store shopping',
        'for_others'        => 'For Others'
    ],
    'payment'               => [
        'bank_transfer'     => 'Bank Transfer',
        'payment_getway'    => 'Payment getway',
        'cash'              => 'Cash',
        'cod'               => 'COD',
        'pos'               => 'POS'
    ],
    'choose_status'         => 'Choose status',
    'choose_delivery'       => 'Choose delivery',
    'choose_payment'        => 'Choose payment',
    'settings'               => [
        'title'             => 'Order',
        'description'       => 'Setting for vat(%)',
        'vat'               => 'Vat(%)',
    ],
    'date_purchase'         => 'Date of purchase',
    'order_status'          => 'Order status',
    "order_date"            => 'Order date',

    'customer_name.required'    => 'Customer name required',
    'customer_email.required'   => 'Customer email required',
    'customer_phone.required'   => 'Phone customer phone required',
    'customer_company.max'      => 'Error max length',
    'customer_address.required' => 'Customer address required',
    'customer_province.required'=> 'Customer province required',
    'customer_district.required'=> 'Customer district required',
    'customer_ward.required'    => 'Customer ward required',
    'delivery_by.required'      => 'Delivery method required',

    'receiver_name.required'    => 'Customer name required',
    'receiver_email.required'   => 'Customer email required',
    'receiver_phone.required'   => 'Phone customer phone required',
    'receiver_company.max'      => 'Error max length',
    'receiver_address.required' => 'Customer address required',
    'receiver_province.required'=> 'Customer province required',
    'receiver_district.required'=> 'Customer district required',
    'receiver_ward.required'    => 'Customer ward required',
    'delivery_date.required'    => 'Delivery date required',
    'delivery_time.required'    => 'Delivery time required'


];
