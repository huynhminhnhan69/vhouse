<?php
    return [
        'name' => 'Vận chuyển',
        'create' => 'Thêm phí vận chuyển',
        'edit' => 'Sửa phí vận chuyển',
        'province/cities'  => 'Tỉnh/thành phố',
        'district/wards'   => 'Quận/huyện',
        'price'            => 'Giá',
        'choose_province/cities'  => 'Chọn tỉnh/thành phố',
        'choose_district/wards'   => 'Chọn quận/huyện',
    ];
