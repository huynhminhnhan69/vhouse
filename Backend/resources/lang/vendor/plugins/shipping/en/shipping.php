<?php

return [
    'name' => 'Shipping',
    'create' => 'New shipping',
    'edit' => 'Edit shipping',
    'province/cities'  => 'Province/cities',
    'district/wards'   => 'District/wards',
    'price'            => 'Price',
    'choose_province/cities'  => 'Choose province or city',
    'choose_district/wards'   => 'Choose district or wards',
];
