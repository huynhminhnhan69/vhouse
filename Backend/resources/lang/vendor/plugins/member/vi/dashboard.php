<?php

return [
    'joined_on'             => 'Joined :date',
    'dob'                   => 'Born :date',

    // Auth translate key
    'email'                 => 'Email',
    'password'              => 'Mật khẩu',
    'password-confirmation' => 'Xác nhận mật khẩu',
    'remember-me'           => 'Ghi nhớ đăng nhập',
    'login-title'           => 'Log In',
    'login-cta'             => 'Log In',
    'register-title'        => 'Đăng ký',
    'register-cta'          => 'Đăng ký',
    'forgot-password-cta'   => 'Quên mật khẩu?',
    'name'                  => 'Name',
    'fullname'              => 'Họ & tên',
    'reset-password-title'  => 'Đặt lại mật khẩu',
    'reset-password-cta'    => 'Đặt lại mật khẩu',
    'cancel-link'           => 'Cancel',
    'logout-cta'            => 'Log Out',

    'header_profile_link'  => 'Profile',
    'header_settings_link' => 'Settings',
    'header_logout_link'   => 'Logout',
    'unknown_state'        => 'Unknown',

    'close'                     => 'Close',
    'save'                      => 'Save',
    'loading'                   => 'Loading...',
    'new_image'                 => 'New image',
    'change_profile_image'      => 'Change avatar',
    'save_cropped_image_failed' => 'Save cropped image failed!',
    'failed_to_crop_image'      => 'Failed to crop image',
    'failed_to_load_data'       => 'Failed to load data',
    'read_image_failed'         => 'Read image failed',
    'update_avatar_success'     => 'Update avatar successfully!',
    'change_avatar_description' => 'Click on image to change avatar',
    'notices'                   => [
        'error'   => 'Error!',
        'success' => 'Success!',
    ],

    // Sidebar
    'sidebar_title'             => 'Personal settings',
    'sidebar_information'       => 'Account Information',
    'sidebar_security'          => 'Security',

    // Account Information
    'account_field_title'       => 'Account Information',
    'profile-picture'           => 'Profile picture',
    'uploading'                 => 'Uploading...',
    'phone'                     => 'Số điện thoại',
    'first_name'                => 'First name',
    'last_name'                 => 'Last name',
    'description'               => 'Short description',
    'description_placeholder'   => 'Tell something about yourself...',
    'verified'                  => 'Verified',
    'verify_require_desc'       => 'Please verify email by link we sent to you.',
    'birthday'                  => 'Ngày sinh',
    'year_lc'                   => 'year',
    'month_lc'                  => 'month',
    'day_lc'                    => 'day',
    'gender'                    => 'Giới tính',
    'gender_male'               => 'Nam',
    'gender_female'             => 'Nữ',
    'gender_other'              => 'Other',

    // Security
    'security_title'            => 'Security',
    'current_password'          => 'Mật khẩu hiện tại',
    'password_new'              => 'Mật khẩu mới',
    'password_new_confirmation' => 'Mật khẩu xác nhận',
    'password_update_btn'       => 'Update password',

    'activity_logs' => 'Activity Logs',

    'oops'             => 'Oops!',
    'no_activity_logs' => 'You have no activity logs yet',
    'actions'          => [
        'create_post'                => 'You created post ":name"',
        'update_post'                => 'You updated post ":name"',
        'delete_post'                => 'You deleted post ":name"',
        'update_setting'             => 'You updated your settings',
        'update_security'            => 'You updated your security settings',
        'your_post_updated_by_admin' => 'Your post ":name" is updated by administrator',
        'changed_avatar'             => 'You changed your avatar',
    ],
    'load_more'        => 'Load more',
    'loading_more'     => 'Loading...',
    'password_incorect' => 'Mật khẩu không hợp lệ',
    'update_profile_success'    => 'Cập nhật hồ sơ thành công'
];
