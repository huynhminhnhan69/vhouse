<?php
    return [
        'required'      => ':attribute không được bỏ trống.',
        'max'           => ':attribute không được dài hơn :max ký tự.',
        'min'           => ':attribute không được ít hơn :min ký tự.',
        'date'          => ':attribute không đúng định dạng.',
        'rules'         => ':attribute không tồn tại trong giá trị mặc định.',
        'numeric'       => ':attribute phải là kiểu số.',
        'exists'        => ':attribute không được bỏ trống.',
        'email'         => ':attribute không đúng định dạng.',
        'unique'        => ':attribute đã tồn tại.',
        'null_in_array' => 'Xin nhập đầy đủ thông tin địa chỉ.',
        'member_id'     => 'Không thể lấy giá trị của memberID.',
        'confirmed'     => ':attribute không trùng khớp.',
        'digits'        => ':attribute phải gồm :size chữ số.',
        'in'            => ':attribute không hợp lệ',
        'regex'         => ':attribute không hợp lệ',
    ];
