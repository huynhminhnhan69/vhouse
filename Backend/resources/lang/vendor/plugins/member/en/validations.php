<?php
    return [
        'required'  => 'The :attribute field is required.',
        'max'       => 'The :attribute may not be greater than :max.',
        'min'       => 'The :attribute may not be less than :min.',
        'date'      => 'The :attribute incorrect date format.',
        'rules'     => 'The :attribute does not exist in the default data.',
        'numeric'   => 'The :attribute must be numeric.',
        'exists'    => 'The :attribute field is required.',
        'email'     => 'The :attribute field invalid format.',
        'unique'    => 'The :attribute already exist.',
        'null_in_array' => 'Please fill enough street, province, district, ward',
        'confirmed' => 'The :attribute does not match.',
        'digits'    => 'The :attribute must include :size digits.',
        'in'        => 'The :attribute is invalid',
        'regex'     => ':attribute is invalid',
    ];
