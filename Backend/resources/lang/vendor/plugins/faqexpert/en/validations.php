<?php
    return [
        'required'  => 'The :attribute field is required.',
        'max'       => 'The :attribute may not be greater than :max.',
        'date'      => 'The :attribute incorrect date format.',
        'rules'     => 'The :attribute does not exist in the default data.',
        'numeric'   => 'The :attribute must be numeric.',
        'exists'    => 'The :attribute field is required.',
    ];
