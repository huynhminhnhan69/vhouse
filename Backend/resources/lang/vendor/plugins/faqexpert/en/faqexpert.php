<?php

return [
    'name' => 'FAQs',
    'create' => 'New FAQs',
    'edit' => 'Edit FAQs',

    'forms' => [
        'placeholder_views'             => 'Views',
        'faqexpert_category'            => 'Faqexpert Category',
		'views'                         => 'Views',
		'name'                          => 'Name',
		'content'                       => 'Content',
		'content_reply'                 => 'Content Reply',
		'customer_email'                => 'Email',
		'customer_phone'                => 'Phone',
		'customer_fullname'             => 'Fullname',
		'placeholder_name'              => 'Name',
		'placeholder_content'           => 'Content',
		'placeholder_content_reply'     => 'Content Reply',
		'placeholder_customer_email'    => 'Email',
		'placeholder_customer_phone'    => 'Phone',
		'placeholder_customer_fullname' => 'Fullname',
		'reply_by'                      => 'Reply By'
    ],
    'actions' => [
    	'create' => 'Create Faq Successfully',
    	'failed' => 'Failed Action. Please try again.'
    ],
    'questions_with_experts'            => 'Ask questions with experts',
    'make_question'                     => 'Make a question',
    'send'                              => 'Send',
    'expert_answer'                     => 'Experts answer',
    'see_more'                          => 'See more',
    'most_view'                         => 'Most view',
    'list'                              => 'List Faqexpert',
    'category'                          => 'Category',
    'health_check'  => [
        'title' => 'Health check',
        'title_result' => 'Health check result',
        'forms' => [
            'name'     => 'Name',
            'dob'      => 'Birthday',
            'd'        => 'Measure Day',
            'gender'   => 'Gender',
            'w'        => 'Weight',
            'l'        => 'Height',
            'male'     => 'Male',
            'female'   => 'Female'
        ],
        'no_advice'    => 'Invalid value. Please re-enter',
        'result'       => 'Result',
        'complete_info'=> 'Please complete your information',
        'health_status'=> 'Assess the health status'
    ],
    'private_category'     => 'You want to ask questions specifically for experts',
];
