<?php

return [
    'name' => 'FAQs',
    'create' => 'Tạo FAQs',
    'edit' => 'Sửa FAQs',
    'forms' => [
        'views'                         => 'Lượt xem',
        'faqexpert_category'            => 'Loại câu hỏi',
		'title'                         => 'Tiêu đề',
		'content'                       => 'Nội dung câu hỏi',
		'content_reply'                 => 'Nội dung trả lời',
		'customer_email'                => 'Email',
		'customer_phone'                => 'Điện thoại',
        'customer_fullname'             => 'Họ và tên',
		'placeholder_title'             => 'Tiêu đề',
		'placeholder_content'           => 'Nội dung câu hỏi',
		'placeholder_content_reply'     => 'Nội dung trả lời',
		'placeholder_customer_email'    => 'Email',
		'placeholder_customer_phone'    => 'Điện thoại',
		'placeholder_customer_fullname' => 'Họ và tên',
		'placeholder_views'             => 'Lượt xem',
		'reply_by'                      => 'Người trả lời'
    ],
    'questions_with_experts'            => 'Đặt câu hỏi với chuyên gia',
    'make_question'                     => 'Đặt câu hỏi',
    'send'                              => 'Gửi',
    'expert_answer'                     => 'Chuyên gia trả lời',
    'see_more'                          => 'Xem thêm',
    'most_view'                         => 'Xem nhiều nhất',
    'list'                              => 'Danh sách câu hỏi',
    'category'                          => 'Thể loại',
    'health_check'  => [
        'title' => 'Kiểm tra sức khỏe',
        'title_result' => 'Kiểm tra sức khỏe kết quả',
        'forms' => [
            'name'     => 'Họ và tên',
            'dob'      => 'Ngày sinh',
            'd'        => 'Ngày cân đo',
            'gender'   => 'Giới tính',
            'w'        => 'Cân nặng',
            'l'        => 'Chiều cao',
            'male'     => 'Nam',
            'female'   => 'Nữ'
        ],
        'no_advice'    => 'Giá trị không hợp lệ. Vui lòng nhập lại',
        'result'       => 'Xem kết quả',
        'complete_info'=> 'Hãy điền đầy đủ thông tin của bạn',
        'health_status'=> 'Đánh giá tình trạng sức khoẻ'
    ],
    'choose_categories'     => 'Chọn loại câu hỏi',
    'private_category'     => 'Bạn muốn đặt câu hỏi riêng cho chuyên gia?',
];
