<?php
    return [
        'required'      => ':attribute không được bỏ trống.',
        'max'           => ':attribute không được dài hơn :max ký tự.',
        'date'          => ':attribute không đúng định dạng.',
        'rules'         => ':attribute không tồn tại trong giá trị mặc định.',
        'numeric'       => ':attribute phải là kiểu số.',
        'exists'        => ':attribute không được bỏ trống.',
    ];
