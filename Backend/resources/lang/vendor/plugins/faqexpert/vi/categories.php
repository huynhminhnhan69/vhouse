<?php

return [
    'create' => 'Tạo mới danh mục',
    'edit' => 'Chỉnh sửa danh mục',
    'menu' => 'Danh mục góc chuyên gia',
    'edit_this_category' => 'Chỉnh sửa danh mục này',
    'menu_name' => 'Danh mục câu hỏi',
    'none' => 'Không',
    'faqexpert_categories'    => 'Danh mục góc chuyên gia'
];
