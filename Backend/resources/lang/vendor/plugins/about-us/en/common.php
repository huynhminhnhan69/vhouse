<?php

return [
    'home' => 'Home',
    'content_404' => 'This may have occurred because of several reasons',
    'title_404' => 'Page could not be found',
    'back_home' => 'Back to home',
    'search' => 'Search',
];
