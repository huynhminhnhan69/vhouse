<?php

return [
    'home' => 'Trang chủ',
    'content_404' => 'Điều này có thể đã xảy ra vì một vài lý do',
    'title_404' => 'Không tìm thấy trang',
    'back_home' => 'Trở về trang chủ',
    'search' => 'Tìm kiếm',
];
