<?php

return [
	'name'           => 'Quản lí Banner',
	'create'         => 'Tạo mới Banner',
	'edit'           => 'Chỉnh sửa Banner',
	'disabled_title' => 'Ẩn hiện Tiêu Đề',
	'banner_type'    => 'Loại Banner',
	'type' => [
		'homepage'         => 'Trang Chủ',
		'contact'          => 'Liên hệ',
		'product'          => 'Sản phẩm',
		'product_detail'   => 'Chi tiết sản phẩm',
		'blog_baby'        => 'Dinh dưỡng chuẩn Châu Âu',
		'blog_baby_detail' => 'Chi tiết dinh dưỡng chuẩn Châu Âu',
		'faq'              => 'Góc chuyên gia',
		'faq_detail'       => 'Chi tiết góc chuyên gia',
		'blog_new'         => 'Tin tức khuyến mãi',
		'blog_new_detail'  => 'Chi tiết tin tức khuyến mãi',
        'about_us'         => 'Về chúng tôi',
        'check_health'     => 'Kiểm tra sức khỏe',
	]
];
