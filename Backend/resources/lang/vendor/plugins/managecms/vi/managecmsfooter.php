<?php

return [
	'name'         => 'Quản lí Footer',
	'create'       => 'Tạo mới Footer',
	'edit'         => 'Chỉnh sửa Footer',
	'add_new'      => 'Thêm mới',
	'footer_links' => 'Quản lí đường dẫn',
	'url_title'    => 'Nội dung',
	'url_link'     => 'Đường dẫn'
];
