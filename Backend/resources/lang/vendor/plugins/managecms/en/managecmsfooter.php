<?php

return [
	'name'         => 'Managecms Footer',
	'create'       => 'New managecms Footer',
	'edit'         => 'Edit managecms Footer',
	'add_new'      => 'Add new',
	'footer_links' => 'Manage Urls',
	'url_title'    => 'Title',
	'url_link'     => 'Url'
];
