<?php

return [
	'name'           => 'Managecms Banner',
	'create'         => 'New managecms Banner',
	'edit'           => 'Edit managecms Banner',
	'disabled_title' => 'Show/Hide Title',
	'banner_type'    => 'Type',
	'type' => [
		'homepage'         => 'Homepage',
		'contact'          => 'Contact',
		'product'          => 'Product',
		'product_detail'   => 'Product detail',
		'blog_baby'        => 'European standard nutrition',
		'blog_baby_detail' => 'European standard nutrition details',
		'faq'              => 'FAQ',
		'faq_detail'       => 'FAQ detail',
		'blog_new'         => 'Blog new',
		'blog_new_detail'  => 'Blog new detail',
        'about_us'         => 'About us',
        'check_health'     => 'Health check',
	]
];
