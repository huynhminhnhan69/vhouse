<?php
    return [
        'description'                               => 'Mô tả',
        'thumbnail'                                 => 'Hình đại diện',
        'product_categories'                        => 'Danh mục sản phẩm',
        'product_code'                              => 'Mã sản phẩm',
        'views'                                     => 'Lượt xem',
        'origin'                                    => 'Xuất xứ',
        'sold_by'                                   => 'Được bán bởi',
        'add_color'                                 => 'Thêm màu',
        'add_size'                                  => 'Thêm kích thước',
        'none'                                      => 'Không có',
        'size'                                      => 'Kích thước',
        'price'                                     => 'Giá',
        'quantity'                                  => 'Số lượng',
        'selling_unit'                              => 'Đơn vị bán'
    ];
?>
