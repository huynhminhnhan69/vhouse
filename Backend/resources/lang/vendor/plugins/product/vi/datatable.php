<?php
    return [
        'search'               => 'Tìm kiếm',
        'paginate'              => [
            'first'             => 'Đầu tiên',
            'last'              => 'Cuối cùng',
            'next'              => 'Sau',
            'previous'          => 'Trước'
        ],
        'zeroRecords'           => 'Không có dữ liệu hiển thị',
        'infoEmpty'             => 'Hiển thị 0 tới 0 trên 0 mục',
        'show'                  => 'Hiển thị',
        'resultPerPage'         => 'Kết quả trên một trang',
        'showPage'              => 'Hiển thị trang',
        'in'                    => 'trong',
        'info'                  => 'Hiển thị _START_ đến _END_ của _TOTAL_ mục',
        "infoFiltered"          => '(Lọc từ _MAX_ tổng số mục)',
        'loadingRecords'        => 'Đang tải...',
        'processing'            => 'Quá trình...',
        'lengthMenu'            => 'Hiển thị _MENU_ mục'
    ];