<?php
    return [
        'list'                      => 'Danh sách loại sản phẩm',
        'name'                      => 'Loại sản phẩm',
        'create'                    => 'Thêm mới loại sản phẩm',
        'edit'                      => 'Chỉnh sửa loại sản phẩm',
        'choose_product_type'       => 'Chọn loại sản phẩm',
        'forms'             =>[
            'description'       => 'Mô tả'
        ],
        'all'                       => 'Tất cả'
    ];
