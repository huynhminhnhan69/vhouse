<?php
    return [
        "name"      => "Xuất xứ",
        "list"      => "Danh sách xuất xứ sản phẩm",
        "create"    => "Thêm xuất xứ sản phẩm",
        "edit"      => "Sửa xuất xứ sản phẩm",
        "choose"    => "Chọn xuất xứ sản phẩm",
        "description"   => "Mô tả nguồn gốc sản phẩm"
    ];
?>