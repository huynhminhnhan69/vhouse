<?php

return [
    'create' => 'Tạo mới danh mục',
    'edit' => 'Chỉnh sửa danh mục',
    'menu' => 'Danh mục sản phẩm',
    'edit_this_category' => 'Chỉnh sửa danh mục này',
    'menu_name' => 'Danh mục sản phẩm',
    'none' => 'Không',
    'product_categories'    => 'Danh mục sản phẩm',
    'choose_categories'     => 'Chọn danh mục sản phẩm',
    'title'                 => 'Tiêu đề',
    'thumbnail'             => 'Hình đại diện'
];
