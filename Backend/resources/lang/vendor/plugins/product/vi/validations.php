<?php
    return [
        'required'      => ':attribute không được bỏ trống.',
        'max'           => ':attribute không được dài hơn :max ký tự.',
        'date'          => ':attribute không đúng định dạng.',
        'rules'         => ':attribute không tồn tại trong giá trị mặc định.',
        'numeric'       => ':attribute phải là kiểu số.',
        'exists'        => ':attribute không được bỏ trống.',
        'between'       => ':attribute phải nằm trong khoảng :min và :max.',
        'before_or_equal'      => ':attribute phải là một ngày trước hoặc bằng ngày kết thúc.',
        'after_or_equal'       => ':attribute phải là một ngày sau hoặc bằng ngày bắt đầu.',
        'unique'        => ':attribute đã tồn tại.',
    ];
