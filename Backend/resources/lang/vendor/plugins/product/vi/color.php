<?php
    return [
        "name"      => "Thuộc tính màu sản phẩm",
        "list"      => "Màu sản phẩm",
        "color"     => "Màu sắc",
        "create"    => "Thêm mới màu",
        "edit"      => "Chỉnh sửa màu",
        "choose"    => "Chọn màu"
    ];
?>