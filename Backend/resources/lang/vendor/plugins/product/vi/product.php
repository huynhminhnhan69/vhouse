<?php

return [
    'name'              => 'Sản phẩm',
    'create'            => 'Tạo mới sản phẩm',
    'edit'              => 'Chỉnh sửa sản phẩm',
    'list'              => 'Danh sách sản phẩm',
    'ecommercial_box'   => 'Liên kết thương mại điện tử',
    'add_new'           => 'Thêm mới',
    'buy_in'            => 'Mua ở',
    'detail_information'=> 'Thông tin chi tiết sản phẩm',
    'product_type'          => 'Loại sản phẩm',
    'related_product'       => 'Các dòng sản phẩm',
    'other_product'         => 'Sản phẩm khác',
    'all'                   => 'Tất cả sản phẩm',
    'asc'                   => 'Giá tăng dần',
    'desc'                  => 'Giá giảm dần'
];
