<?php
return [
    "name"      => "Product color attributes",
    "list"      => "List product colors",
    "color"     => "Colors",
    "create"    => "Create new color",
    "edit"      => "Edit new color",
    "choose"    => "Choose color"
];

?>