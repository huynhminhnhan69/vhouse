<?php
    return [
        'required'  => 'The :attribute field is required.',
        'max'       => 'The :attribute may not be greater than :max.',
        'date'      => 'The :attribute incorrect date format.',
        'rules'     => 'The :attribute does not exist in the default data.',
        'numeric'   => 'The :attribute must be numeric.',
        'exists'    => 'The :attribute field is required.',
        'between'   => 'The :attribute must be between :min and :max digits.',
        'before_or_equal'      => 'The :attribute must be a date before or equal to end day.',
        'after_or_equal'       => 'The :attribute must be a date after or equal to start day.',
        'unique'    => 'The :attribute already exist.',
    ];
