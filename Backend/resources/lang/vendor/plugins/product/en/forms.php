<?php
    return [
        'description'                               => 'Description',
        'thumbnail'                                 => 'Thumbnail',
        'product_categories'                        => 'Product categories',
        'product_code'                              => 'Product code',
        'views'                                     => 'Views',
        'origin'                                    => 'Origin',
        'colors'                                    => 'Colors',
        'sold_by'                                   => 'Sold by',
        'add_color'                                 => 'Add color',
        'add_size'                                  => 'Add size',
        'none'                                      => 'None',
        'size'                                      => 'Size',
        'price'                                     => 'Price',
        'quantity'                                  => 'Quantity',
        'selling_unit'                              => 'Selling unit'
    ];
?>
