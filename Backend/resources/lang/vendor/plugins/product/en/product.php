<?php

return [
    'name'                  => 'Product',
    'create'                => 'New product',
    'edit'                  => 'Edit product',
    'list'                  => 'List product',
    'ecommercial_box'       => 'E-commercial link',
    'add_new'               => 'Add New',
    'buy_in'                => 'Buy In',
    'detail_information'    => 'The detail information of product',
    'product_type'          => 'Product type',
    'related_product'       => 'Product type',
    'other_product'         => 'Other products',
    'all'                   => 'All products',
    'asc'                   => 'Price increase',
    'desc'                  => 'Price descending'
];
