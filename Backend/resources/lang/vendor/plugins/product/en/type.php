<?php
    return [
        'list'                  => 'List product type',
        'name'                  => 'Product types',
        'create'                => 'Create product type',
        'edit'                  => 'Edit product type',
        'choose_product_type'   => 'Choose product type',
        'forms'                 => [
            'description'           => 'Description',
        ],
        'all'                   => 'All'
    ];
