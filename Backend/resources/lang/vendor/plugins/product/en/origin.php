<?php
    return [
        "name"      => "Origin products",
        "list"      => "List origin products",
        "create"    => "Add origin product",
        "edit"      => "Edit origin product",
        "choose"    => "Choose origin products",
        "description"   => "Origin description"
    ];
?>