<?php
    return [
        'search'               => 'Search',
        'paginate'              => [
            'first'             => 'First',
            'last'              => 'Last',
            'next'              => 'Next',
            'previous'          => 'Last'
        ],
        'zeroRecords'           => 'There is no record',
        'infoEmpty'             => 'Showing 0 to 0 of 0 entries',
        'show'                  => 'Showing',
        'resultPerPage'         => 'result(s) per page',
        'showPage'              => 'Showing page',
        'in'                    => 'in',
        'info'                  => 'Showing _START_ to _END_ of _TOTAL_ entries',
        "infoFiltered"          => '(filtered from _MAX_ total entries)',
        'loadingRecords'        => 'Loading...',
        'processing'            => 'Processing...',
        'lengthMenu'            => 'Show _MENU_ entries'
    ];