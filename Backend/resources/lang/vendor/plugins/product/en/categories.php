<?php

return [
    'create' => 'Create new category',
    'edit' => 'Edit category',
    'menu' => 'Categories',
    'edit_this_category' => 'Edit this category',
    'menu_name' => 'Categories',
    'none' => 'None',
    'product_categories'    => 'Product Categories',
    'choose_categories'     => 'Choose product categories',
    'title'                 => 'Title',
    'thumbnail'             => 'Thumbnail'
];
