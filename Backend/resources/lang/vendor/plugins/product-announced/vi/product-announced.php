<?php

return [
    'name'      => 'Sản Phẩm Công Bố',
    'create'    => 'Thêm mới sản phẩm công bố',
    'edit'      => 'Cập nhật sản phẩm công bố',
    'list'      => 'Danh sách sản phẩm công bố',
    'forms'     => [
        'thumbnail'                 => 'Thumbnail',
        'number_of_publication'     => 'Số công bố',
        'date_of_publication'       => 'Ngày công bố'
    ],
    'metabox'   => [
        'icon'          => 'Biểu tượng',
        'choose_icon'   => 'Chọn biểu tượng',
        'document_name' => 'Tên tài liệu sản phẩm',
        'file_path'     => 'Đường dẫn tài liệu',
        'choose_file'   => 'Chọn tập tin',
        'product_document'  => 'Tài liệu thông tin công bố'
    ]
];
