<?php

return [
    'name'      => 'Product Announced',
    'create'    => 'New product announced',
    'edit'      => 'Edit product announced',
    'list'      => 'List product announced',
    'forms'     => [
        'thumbnail'                 => 'Thumbnail',
        'number_of_publication'     => 'Number Of Publication',
        'date_of_publication'       => 'Date Of Publication'
    ],
    'metabox'   => [
        'icon'          => 'Icon',
        'choose_icon'   => 'Choose Icon',
        'document_name' => 'Document name',
        'file_path'     => 'File path',
        'choose_file'   => 'Choose file',
        'product_document'  => 'Product documents'
    ]
];
